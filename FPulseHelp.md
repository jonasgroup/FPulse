# FPulse Manual
© 2002-2005 UF. All rights reserved.
Manual version: 2005 November
FPulse version: 3.06

## Changelog
2024-Apr: Remark: This version is a text copy (i.e. without formatting) from the FPulse.ihf file, and was manually formatted into a markdown format using ghostwriter, and then converted to html. The formatting might not represent the original file (Apr 2024).


## Introduction
FPulse is a data acquisition program developed for (but not limited to) electrophysiology.
It is designed for the CED 1401 series acquisition hardware running under Windows.
It includes a quite elaborate stimulus generation.
ADCs, DACs and digital outputs are controlled by a simple script language.
A few basic online-analysis functions (mainly for electrophysiology) are included.
The package is designed to fulfill the major requirements of both the pulsed and the continuous acquisition mode.  Acquisition time is limited only by the hard disk capacity while maintaining a timing precision below a microsecond.
The data files written are using the CFS file format.
The telegraph signals of the AxoPatch 200A, 200B and 700A are recognised.



This manual is divided into the following sections.
The chapter  FPulse Installation  guides through the connection of the Ced1401 hardware and lets you set up the necessary directories, files and links.
FPulse Quick tour  demonstrates much of the functionality of FPulse in a few minutes.
As user written  FPulse Script files  control stimulus and data acquisition they will be described in detail below.
The  FPulse Panels  sections describes the controls of every panel. It tells what actions will be executed when buttons are pressed and how special settings affect system behavior.
HowTo  shows how some common tasks are accomplished.
Pitfalls  addresses potential difficulties the user may encounter and how to circumvent them.
Problems  describes difficulties for which no satisfactory solution has yet been found.



#	FPulse Installation

## Installation of the CED1401

Install the Ced software using the default  directories C:\1401\utils and C:\1401\windrv.
After the CED1401 has been installed check that it is working.
Run the program  Try1432.exe  which is supplied by CED.

If you are using a POWER1401 you should use the USB interface because it does not require an interface card. If you have a PCI interface card installed already you may use it as well.

When you want to switch between the interfaces follow this procedure:
When switching from PCI to USB:
Deactivate the CED1401 PCI interface 'System -> ..Device Manager..'
Turn the computer off
Turn the CED1401 off
Disconnect the PCI interface cable
Connect the USB cable
Turn CED1401 on
Turn computer on
Run the program  Try1432.exe  which is supplied by CED.
If the CED1401 fails (this happens sometimes) turn computer and CED1401 off and on again.

When switching from USB to PCI:
Turn the computer off
Turn the CED1401 off
Disconnect the USB cable
Connect the PCI interface cable
Turn CED1401 on
Turn computer on
Activate the CED1401 PCI interface 'System -> ..Device Manager..'
Run the program  Try1432.exe  which is supplied by CED.
If the CED1401 fails (this happens sometimes) turn computer and CED1401 off and on again.

If you are using a 1401Plus you should use it with a PCI interface card.
Using an ISA interface is possible but is strongly discouraged as the achievable data rates are about 4 time lower than with a PCI interface.

Only if you are using a Standard1401 an ISA interface makes sense since the Standard1401 is slow by itself.


## Installation of IGOR

Software requirements
Windows 98, Windows Me, Windows NT, Windows 2000 or Windows XP
Windows 95 will not work with Igor5 and FPulse.

Hardware requirements
Minimum screen resolution is 1024 x 768 pixels, recommended is 1280 x 1024.
Computer speed minimum 500MHz, recommended 1GHz or faster

For FPulse2.35 or higher you must have installed Igor5.02 or higher.
Updating Igor by an internet download is easy: 'Help' -> 'Updates for Igor..'
The rest is almost automatic.
This simple download mechanism works only for minor Igor updates (the number behind the dot).
Major Igor updates (the number in front of the dot) cannot be downloaded in that manner but must be bought from WaveMetrics. In other words: when you have any Igor version 5.xx installed you can update from WaveMetrics web site free of charge until Igor 6.0 will be released.


## Installation of FPulse (Version >=2.38)
Installing FPulse Version 2.38 or higher requires an Igor version 5.02 or higher . If your Igor version is at least 5.0 you can easily update by Internet.
Select 'Help' -> 'Updates for Igor..'  ( or 'WaveMetrics Home Page' ) -> 5.02 (or higher)
-> 'Stand alone Updaters' -> 'Main FTP site'  (be sure to download the Windows version) .

Check that there is a file  'FPulse_Install.pxp'  and a directory  'FPulse' in the root of the FPulse-CDROM.
If you are installing by Internet or by a (possibly zipped) Email attachment this file  'FPulse_Install.pxp'  and the directory  'FPulse' must be placed in the root directory of hard disk C: .
Although principally any hard disk will do for historical reasons (and to maintain compatibility with existing installations) a directory  'UserIgor'  will be created on drive C: so this drive must provide at least some MByte of free disk space.

Proceed as follows:

-	Quit  IGOR

-	Double-click on   'FPulse_Install.pxp'

An installation dialog panel will appear.

- 	Select  'Install latest version' .

This applies only for a first-time installation, if there is already an FPulse version found, this version must be deinstalled first. The steps are described below.
FPulse versions <= 2.37 are not recognized as a valid previous installations, so they do not have to be deinstalled.

Generally any reinstallation of  'FPulse' requires 2 steps. After the first execution of  'FPulse_Install.pxp'  Igor must be quitted,  'FPulse_Install.pxp' must be executed a second time. This is required by Igor which checks and installs links to procedure, help and Xop files only at startup and locks these files at runtime so that updating them is impossible in one step.

Only for the very first installation of  'FPulse'  it is sufficient to execute 'FPulse_Install.pxp' only once.
To start  'FPulse'  however, Igor must quitted first.

When installing any  FPulse version greater or equal 2.38, a backup is also installed. This allows you to switch back at any time to any previous version should need arise. The previous versions are never deleted when you select  'Remove FPulse'  . Script files also are not deleted when you select  'Remove FPulse' .

Caution:
Only if you really want to get rid of  all traces of a previous 'FPulse'  installation then you must delete the directory 'C:UserIgor' manually.  'C:UserIgor' contains scripts files, display configuration files and previous FPulse versions.

If you want to change the drive where FPulse is installed, you must execute  'FPulse_Install.pxp'  and select  'Remove FPulse' . You then must delete all previous FPulse versions or you must move them to the new drive. These are all directories which match  'HD:UserIgor:FP*' .

Watch out not to delete the directory  ' 'HD:UserIgor:Scripts' !

All previous FPulse versions must be deleted before a new installation drive can be selected.
To avoid a mixup between versions any new version will automatically be installed in that drive where a previous version has been found.

When the message
'FPulse has been installed'  or  'FPulse has been removed'  appears you must quit Igor.
If you have installed FPulse you will find it in the 'Analysis' menu after you restart Igor.

You will find the  FPulse manual  under  'Help' -> 'Help Topics' -> 'FPulse ...' .



## Installation details

Library and DLL setup
There are three library files that must be copied:
The CED library file   Use1432.dll   must be copied from C:\1401\utils  into the  WindowsXX\Sytem32  directory. At the time of this writing  Use1432.dll  is a custom version modified by Ced (Tim Bergel) for the use with Igor and FPulse.
This version has 28730 bytes and dates from Feb 23rd, 2004 , 15:57h .

The CED library file   CFS32.dll   must be downloaded from CED.CO.UK (if not found in C:\1401\ ) and copied  into the  WindowsXX\Sytem32  directory.

The CED library 1432ui.DLL (found in C:\1401\WinDrv) must perhaps alo be copied into the WindowsXX\Sytem32  directory.

Make sure that the 1401 directory containing the loadable CED commands is located at
C:\1401 . If the directory is not found at C:\1401 the U14Ld error  -540  will result.

Directory setup
At the time of this writing the directories have to be installed manually by the user.

Make a directory C:\EPC\Data .
This is the standard path for experimental data. It is defined in the script file and can be changed there.
The directory C:\EPC\Data\EvalCfg will be created automatically .

Make a directory C:\UserIgor\Ced .
This is the standard directory for the FPulse program. It cannot be changed.

Copy the file  'ReleaseXXX.exe'  from the floppy disk into the directory C:\UserIgor\Ced .
Unzip it by double-clicking it.

Make a directory C:\UserIgor\Ced\Scripts .
The also needed directory C:\UserIgor\Ced\Scripts\Tmp  will be created automatically .

Needed links
At the time of this writing the links have to be created manually by the user.
When all files have been copied you must make a link from the source
-  ...WaveMetrics \ Igor Pro Folder \ More Extensions \ Utilities \ GetInputState.xop
	     to the target folder	...WaveMetrics \ Igor Pro Folder \ Igor Extensions
-  Ced1401.xop	     to the target folder	...WaveMetrics \ Igor Pro Folder \ Igor Extensions
-  FPMenu.ipf	     to the target folder	...WaveMetrics \ Igor Pro Folder \ Igor Procedures
-  Ced1401 Help.ihf    to the target folder	...WaveMetrics \ Igor Pro Folder \ Igor Help Files
-  FPulse.ihf	     to the target folder	...WaveMetrics \ Igor Pro Folder \ Igor Help Files
( Click the source file  in the Windows Explorer  with the right mouse button and drag or copy the link into the target folder. You may also rename the link by removing 'Verknüpfung mit ' from the file name )


### Installing an FPulse update (Version <=2.39)

It is a good idea to keep the previous FPulse in its zipped version.
Updating is easily accomplished by unzipping the new version in the .\UserIgor\Ced directory.
Reverting to the previous version is done by unzipping it in the same .\UserIgor\Ced directory.
Files not belonging to the current unzipped version are simply overwritten.


##	FPulse Quick tour
Start Igor.
In the history window a message is printed confirming that the file CED1401.XOP has been loaded.
If you have no MultiClamp amplifier connected a  'MultiClamp not found'  warning will be issued.
Select the menu items    'Analysis -> FPulse' .
The main FPulse control panel will pop up on the right side of the screen.
Press  'Script file'  and select a file with the desired the stimulus and the acquisition parameters.
After a few seconds the stimulus will be displayed in a graph. To speed up loading the stimulus display may be turned off by default. To turn it on : 'Stimulus' -> 'Display stimulus: on' .
The script text will possibly appear in an editor window. To reduce clutter on the screen the text display may be turned off or on by pressing: 'Edit Script/Hide Script' .
Probably the graph and the editor window will be covered by many acquisition windows.
You can hide a group of acquisition windows by  opening the panel  'Acq windows' , the selecting the source channel tab and unchecking 'Current ,  Many superimposed' . You can hide individual acq windows by closing them.
If you want to change the stimulus you may now edit the script and then press  'Apply' .
If the stimulus is OK you may start stimulus output and data acquisition by pressing  'Start' .
The 'Start'  button will be disabled, the acquisition data will be written into a file until the message  'Finished acquisition'  is displayed in the history window.
To view the acquired data you may then press  'Read CFS data -> Current acq...' .
This finishes the quick tour of FPulse.



##	FPulse Script Files

A script file is a short text file describing the stimulus signal, data aquisition parameters and the hardware setup.

A script file starts with one connection section  which is followed by one or more stimulus blocks.

The  Connection Section  contains Input / Output information like DAC channel number and name,
ADC channel numbers and names, telegraph channel numbers and the sample interval.

The Stimulus Block describes the output pattern of the Dac, the pulses of the digital ouputs and which episodes of the data are to be stored.

The stimulus block consists of stimulus elements like  Segment , Blank , Ramp , Expo and StimWave  enclosed by  Stimulus control structures  like  Frames  and  Sweeps .
In its simplest form the stimulus is described as a series of  segments defined by a duration and an amplitude. More elaborate forms are Ramps, MultiExponential functions or arbitrary signals. The latter are fed into the stimulus as IGOR waves.


## Script Syntax
The script file is line-oriented. Each line starts with a main keyword, followed by a colon and a list of sub keyword-value pairs specifying the main key. The lines can have any length.
They can contain spaces and tabulators which are ignored.
The indentation is not required by the syntax, it just makes the script better readable.
There can be only one main keyword on each line, so after the last sub keyword-value a Carriage Return (newline character) is needed.
Comments start with //, which end the executable part of a script line. The comment continues to the end of the line. There is no way to insert a comment in the middle of a script line. Comments are ignored.
Script keywords are case-sensitive so upper-case and lower-case must be correct to avoid errors.
All keywords are described in detail below.

Note:	The script file syntax is designed to be acceptable for both new and experienced users. This concerns the extent to which keywords are used: each number having a keyword will be helpful for the first time users but annoying to others because script file lines will get very long, while combining numbers into groups without descriptive keywords makes script lines shorter but much less readable. The compromise chosen is to to supply a unique keyword for most entries (especially those rarely used) but to combine some (much used) entries into a group having just one keyword. For these groups the order of the entries is fixed and important. Entries in a group can be missing (in that case they are replaced by a default) but the order must be maintained by supplying the comma.
	A single value keyword is for example  'Chan'   or  'Gain'  .
	'Amp'  is a group keyword whose required parameter is the magnitude of the stimulus, additional optional parameters are a scaling factor and an increment/decrement.


Let's have a look at a simple script file to illustrate some of the features.


	PULSE
	Protocol:    Name = "DemoSimple"
	Dac:         Chan = 0; SmpInt = 100; Gain =50; Name=Stim0; Units=mV
	Adc:         Chan = 0; SmpInt = 100; TGChan=5; Name=Adc0;  Units=pA

	Frames:      	N    = 2
	   Sweeps:     N    = 3
	      Segment:	Dac = 0, 1; Amp = 80,1,-1;
	      Blank:	Dac = 0, 1; Amp = 60		// InterSweepInterval
	   EndSweep
	   Blank:		Dac = 0, 3			      	// InterFrameInterval with Amp=0
	EndFrame





Connection Section

The first lines between  'PULSE'  and  'Frames'  make up the connection section.
In the following all possible parameters and keywords are described even if they are omitted in the above sample script.

The connection section starts with

	Protocol :	Name = StimulusName   ;

		Name	is optional.
			It should describe the stimulus and it is stored in the CFS data file.


	Dac :	Chan=0; SmpInt= 100; Name= MyDac0; Units= mV; Gain= 50; RGB= (4000,0,4000);

		Chan 	is required.
			It is the CED1401 connector where the stimulus is output.
		SmpInt	is required and must be the same for all DAC and ADC channels.
			It is the time interval between DAC updates in micro seconds
			and must be an integer.
		Name	is optional.
			It is an arbitrary name used in the graphs and stored in the Cfs data file.
			Keep it short or it may cover parts of other axis.
 		Units	are optional.
 			They are arbitrary and only used when printing axis in graphs.
			Their actual values are not used when processing data.
 		Gain	is required.
			It converts the stimulus magnitude as specified in stimulus elements like  				Segment , Blank , Ramp , Expo and StimWave
			to the appropriate voltage or current at the pipette including all
			intermediary gains in the stimulus path of the Ced and the AxoPatch.
			The values to be used are
			voltage clamp mode : 20 mV / V		Units : mV 	Gain :  50
			current clamp mode :  2 nA / V		Units : nA	Gain :  0.5
			current clamp mode :  2000 pA / V	Units : pA	Gain :  0.0005
		RGB	is optional.
			It specifies the color of the traces in the graphical windows.
			The three numbers descibe red, green and blue. Each number
			may range from 0 to 65535 determining the intensity of this color.
			See: Trace Colors


	Adc	:	Chan	= 0; 	SmpInt= 100; Name= ;Units= mV; RGB=(0,53248,0);  ....
		....Gain= 100;	TGChan = 5; TGMCChan= 1;

		Chan 	is required.
			It is the CED1401 connector where the measured signal is output.
		SmpInt	is required and must be the same for all DAC and ADC channels.
			It is the sampling interval in micro seconds and must be an integer.
		Name	is optional.
			It is an arbitrary name used in the graphs and stored in the Cfs data file.
			Keep it short or it may cover parts of other axis.
 		Units	are optional.
 			They are arbitrary and only used when printing axis in graphs.
			Their actual values are not used when processing data.
		RGB	is optional.
			It specifies the color of the traces in the graphical windows.
			The three numbers descibe red, green and blue. Each number
			may range from 0 to 65535 determining the intensity of this color.
			See: Trace Colors
 		Gain	or  TGChan  or  TGMCChan is required and only one is allowed .
			It converts the acquired signal so that the correct magnitude is stored in 				the file and displayed in the graphs.
			This value is used initially when you control the gain manually i.e. when
			there is no telegraph connection.
			Controlling the gain manually is not recommended as in this mode you
			must immediately duplicated any gain change at the amplifier by a
			corresponding change in  'FPulse'  -> 'Gain' .
		TGChan  or  TGMCChan   or   Gain  is required and only one is allowed .
			It is the CED1401 ADC channel number which is connected to the
			telegraph output of the amplifier.
		TGMCChan  or  TGChan   or   Gain  is required and only one is allowed .
			It is (software) telegraph channel number ( 1 or 2 ) of an
			AxoPatch MultiClamp Amplifier. No cable connection is necessary when
			using this amplifier.

	Note:	When using either of the 2 telegraph modes any gain change at the amplifier is 	automatically processed by FPulse. The manual gain setting in FPulse is then 	disabled.

	Note:	During acquisition the gain modes, the telegraph connections, the controlled 	channels and the current gains are displayed for all acquired channels at the right 	edge of the  'StatusBar' at the bottom.


Although not used in the sample script above the keyword controlling the PoverN display is also described right here.

	PoN	:	Src =  Adc0;	Name =  MyPoN; 	Units = pA;	 RGB =(57344,0,0);
		Src 	is required.
			It is the Adc data channel which requires P over N  correction processing.
			All other parameters are the same as described for 'Adc'  and  'Dac' .



Stimulus Block
The stimulus block consists of stimulus elements like  Segment , Blank , Ramp , Expo and StimWave  enclosed by  Stimulus control structures  like  Frames  and  Sweeps  .


Dac Keyword in Stimulus Block
Segment, Blank, Ramp, Expo and Stimwave have a common  'Dac'  keyword.
	Segment/Blank,Ramp..  : Dac =  Ch, Dur, A/R,Del ;
The parameters are channel number, duration of the element in milliseconds, a timing mode specifier which can be  'A'  (=absolute) or  'R'  (=relative)  and starting delay in milliseconds.
The mode specifier  'A'  or  'R'  is only needed for a second, third or fourth Dac, not for the main Dac.
The main Dac is the one which is defined first in the Connection section, it is not necessarily the Dac with the channel number 0 although by convention Dac0 is used as main Dac.  Absolute timing mode means that the start of the element is referred to the script start.  Relative timing mode means that the start of element is referred to the start of the main Dac element which precedes the relative element.
See Also
Multiple DACs , Absolute and relative timing of multiple DACs

Dig Keyword in Stimulus Block

	Segment, Blank, Ramp, Expo and Stimwave have a common  'Dig'  keyword.
	Segment/Blank,Ramp..  : Dig = Ch, Dur, Del, DDur, DDel

This keyword is optional.
It allows to produce digital output pulse (TTL compatible) on any digital output channel of the Ced1401.
The parameters are
-  digital output channel number
-  duration of the digital high (~4V) pulse in milliseconds
-  delay of the digital pulse referrred to the start of current stimulus element Dac output
-  delta duration
-  delta delay
The last 2 parameters are the delta values for automatic increment or decrement which will come into effect when the next frame will be applied  (see: Auto-increment ) .
See Also
Digital Output Pulses

### Segment

A segment is the most common stimulus element. It supplies an output pulse of constant magnitude for a certain period of time on one of the DAC outputs.
It starts with the  'Dac'  keyword.  See :  Dac Keyword in Stimulus Block .
	Segment  : Dac =  Ch, Dur, A/R,Del ;  Amp = Amp, Scl, DAmp, DDur
A  'Segment'  is in its basic form sufficiently defined by the channel number, the output voltage and the duration.
	Segment :  Dac = 1, 10;	Amp	= -80
The preceding line specifies that  DAC number 1 should output a voltage of -80 mV for a duration of 10 milliseconds. The voltage is referred to the pipette, assuming that gain and units have been set correctly.
In an extended form one could specify an additional scaling factor (rarely used) and an automatic amplitude decrement or increment.  See:  Auto-increment
	Segment :  Dac = 1, 10;	Amp	= -80,2
This line would output a voltage of -160 mV.
	Segment :  Dac = 1, 10;	Amp	= -80,1,10
	Segment :  Dac = 1, 10;	Amp	= -80,,10
Both lines would output a voltage of -80 mV in the first frame, in the next frame -70 mV, in the third frame -60 mV and so on.
The last parameter can be used to shorten or prolong the pulse duration automatically by a fixed duration when advancing from frame to frame.


### Blank

A  'Blank'  is much like a  'Segment'  but has some special properties. The parameters and the DAC output are the same as those of a  'Segment' . The important difference is that during a 'Blank' period no data are stored on disk. Another difference is that no PoverN correction takes place.
Although an  'Amp'  can be specified in many cases this output voltage will be zero. In this case it is common practice to omit the  'Amp = 0'  entry completely thereby increasing readability of the script.
As a  'Blank'  separates the stored periods in one sweep, frame or block from the stored period of the next sweep, frame or block they are also called  PreSweepInterval , InterSweepInterval ,  InterFrameInterval  or  InterBlockInterval .

#### PreSweepInterval
A PreSweepInterval is a  'Blank'  line appearing  immediately after the  'Sweeps'  line.
If PoverN is specified in the script or block then this correction applies also for the PreSweepInterval.
Of course this is only of importance if an output voltage different from 0 is specified.
The  PreSweepInterval has no special properties. It is a  'Blank'  line like any other  'Blank'  line.  It is introduced only to make the conversion from 'Pascal Pulse Protocols' easier.

#### InterSweepInterval
An InterSweepInterval is a  'Blank'  line appearing  immediately before the  'EndSweep'  line.
If PoverN is specified in the script or block then this correction applies also for the InterSweepInterval.
Of course this is only of importance if an output voltage different from 0 is specified.
The  InterSweepInterval has no special properties. It is a  'Blank'  line like any other  'Blank'  line.  It is introduced only to make the conversion from 'Pascal Pulse Protocols' easier.

#### InterFrameInterval
An InterFrameInterval is a blank segment not stored on disk which can be inserted between Frames.
It has special properties and special requirements:
	-   it is  NOT  PoverN corrected (that is its amplitude is not inverted and scaled to supply          		    correction pulses) even if it belongs to a script or block which is PoverN corrected.
	-  the  'Blank'  line must appear immediately after the 'EndSweep'  and before the  'EndFrame'  line.
	-  it can be assigned any amplitude, if no amplitude is assigned then 0 is assumed

#### InterBlockInterval
An InterBlockInterval is a blank segment not stored on disk which can be inserted between Blocks.
It has special properties and special requirements:
-  it is  NOT  PoverN corrected (that is its amplitude is not inverted and scaled to supply
   correction pulses) even if it belongs to a script or block which is PoverN corrected.
-  the  'Blank'  line must appear immediately after the  'EndFrame'  line.
-  it can be assigned any amplitude, if no amplitude is assigned then 0 is assumed
These are the only blank intervals allowed i.e. do not insert 'Blank' lines in the middle of a sweep.
See below a script demonstrating the use of these intervals.


	// Demonstrates the InterSweepInterval, InterFrameInterval and InterBlockInterval  using  2 blocks

	// Syntax:  [ Dig =  Ch, Dur, Del, DDur, DDel ]
	// Syntax: Segment/Blank..  :Dac =  Ch, Dur, A/R,Del ;  Amp	= Amp, Scl, DAmp, DDur

	// todo.......



### Ramp
A  'Ramp'  is used to generate a rising or falling ramp-like stimulus.
It starts with the  'Dac'  keyword.  See :  Dac Keyword in Stimulus Block .
A  'Ramp'  element  shares many common properties with  'Segment' . Syntax and parameters are identical. The difference is that a voltage not with constant amplitude but rather a steadily varying voltage with constant slope is output. The  'Amp'  keyword means the voltage reached after the specified duration at the end of the ramp. The ramp starting voltage is not expressively defined, it is the voltage at the end of the preceding element.

### Expo
An  'Expo'  element is used to build a multi-exponential stimulus.
It starts with the  'Dac'  keyword.  See :  Dac Keyword in Stimulus Block .
The  'Dac'  keyword is followed by the  'AmpTau'  keyword.

	Expo :  Dac = Ch, Dur, A/R, Del;  AmpTau = Amp1, TauRise1, TauDecay1, deltaT1, Scl1
            [ [ / Amp2, TauRise2, TauDecay2, deltaT2, Scl2 ] / ... ]

The output function is

		f(t)   = 0					  for t < deltaT

and

		f(t)   = Amp * ( exp( -t /  TauRise ) - exp( -t / TauDecay ) ) ) * Scl     for t >= deltaT

Multiple output functions can be added. The parameter groups must be separated by a slash.


### StimWave
The keyword StimWave allows insertion of arbitrary waves into the stimulus protocol which will be output by the DAC.
It starts with the  'Dac'  keyword.  See :  Dac Keyword in Stimulus Block .

	Stimwave  : Dac = Ch, Dur, A/R,Del ;  Wave = wave file name, yScl, yOfs, xScl

FPulse accepts stimwaves in three file formats.

1.  Two column ASCII data
The first format is a two-column ASCII file containing time as a first column and voltage as a second column, both as text separated by spaces or tabs. This is the file format which has been used in the APWave.xxx files recognized by the Pascal Pulse program.
Any file name is allowed but the extension must NOT be 'IBW' .
The first line is a header line containing a description of the two columns. This line must be present but is ignored in the present version of FPulse.
In the present version the absolute time values are also ignored but the time spacing is recognized.
That means the voltage value of the second line (the first data line) is output at the time following the last point of the preceding element (Segment,Ramp,StimWave,Expo,Blank) in the script file.
If the time spacing of the data in the stimwave file does not match the Dac sample interval specified in the script file, the voltage values are linearly interpolated. This gives you the benefit of not having to space the time values equidistantly in the stimwave file. To keep the file small you can omit data points in linear regions of the voltage change.
If you want to avoid voltage errors in nonlinear regions introduced by the linear interpolation the time spacing in the stimwave file should be equal to or an integral multiple of the Dac sample interval.

2.  One column ASCII data
The second format is a one-column ASCII file containing only the voltage.
Any file name is allowed but the extension must NOT be 'IBW' .
The first line is a header line containing any comment. This line must be present but is ignored in the present version of FPulse.
The voltage values are output at the rate specified as Dac sample interval in the script file.

3. IGOR binary wave format
The third format is the IGOR binary wave format. The extension must be 'IBW' .
The stimulus wave can principally be data from any source.
You can use any built-in Igor function to construct an arbitrary stimulus.
As an example, see : Construct a sinusoidal wave.
Often the stimulus will be extracted from a recorded trace. See : Extract a StimWave.
In both cases the only requirement is that Igors proprietary binary wave format is used.
The stimulus wave is inserted maintaining its timescaling  even if recorded with a different sample interval. In this case the values are interpolated using a cubic spline.
The parameters yOfs and xScl allow shifting the stimulus wave in the vertical direction and stretching/shrinking in the horizontal direction.
Only in the IBW format yOfs and xScl are recognized.
Be careful when renaming IBW files because IGOR stores the wave name independently of the name of the IBW file in which the wave is contained  and does NOT rename the wave when the file name is renamed. This can much later lead to considerable confusion when you define a new wave inadvertently using a wave name which IGOR has continued to use internally although you had intended to resolve a name conflict by renaming the file.


### Stimulus Control Structures
Any number of stimulus elements can be combined in any order if the limitations concerning  'Blank'  elements are respected.
To apply the same stimulus repeatedly one could copy the appropriate script lines a few times.
Although in rare cases it has to be done this way this is generally not an elegant solution. FPulse has 5 types of control structures for flexible stimulus construction while avoiding the need to duplicate lots of script lines.
The 5 nesting levels for stimulus elements are  Loop , Sweeps , Frames , Block and Protocol .
They can only be used in this order,  'Loop' being used as the innermost construct and  'Protocol'  as the outermost.

#### Loop
The  'Loop' - 'EndLoop'  construct is just for saving writing effort, the lines within the loop are internally simply repeated the specified number of times. Due to the limitations of  'Blanks'  these are not allowed in loops except for the useless case of just 1 loop .

Each stimulus block contains one or more frames which in turn consist of one or more sweeps.
The lines between the   'Sweeps'  and the  'EndSweep'  keyword make up the stimulus which is repeated frames x sweeps times.

	Frames		:	N =    1;
	Sweeps	:	N =    1;	PoN =    0;	CorrAmp =    0;
		Blank	:	Dac = ...;	Amp		=  ...;	Dig	=  ...;
		Segment	:	Dac = ...;	Amp		=  ...;	Dig	=  ...;
		Ramp	:	Dac =  ...;	Amp		=  ...;	Dig	=  ...;
		StimWave:	Dac =  ...;	Wave	=  ...;	Dig	=  ...;
		Expo	:	Dac =  ...;	AmpTau	=  ...;	Dig	=  ...;
		Loop:	N =    2;
			Segment	:	Dac =  ...;	Amp		=  ...;	Dig	=  ...;
			Segment	:	Dac =  ...;	Amp		=  ...;	Dig	=  ...;
		EndLoop
	EndSweep
	EndFrame


#### Sweeps
The basic unit of data for internal processing, display and storage is the sweep.
Historically  sweeps  were introduced to provide artefact correction for pulsed data.
For this purpose only the first sweep stimulus of each frame is used for measuring. All following sweep stimuli are derived from the first sweep. They are opposite and smaller in amplitude so that when the stimulus response of the first and the correction sweeps are added the capacitive artefact is cancelled to a large extent.  See:  P over N .
To run this mode  'PoN'  must be set to 1  and  the  sweep script line must read

	Sweeps	:	N = s;	PoN = 1

where s is an integer  number >= 2 .  In the PoverN mode the minimum number of sweeps is 2  giving 1 measuring and 1 correction sweep.  A more often used value is 5 leading to the common P over 4 procedure. The relationship between the number of sweeps  s  and the correction amplitude  Amp_corr  relative to the measuring amplitude is fixed and is

	Amp_corr = -1 / ( s - 1 ) .

This allows to directly add all sweeps of a frame without further scaling for artefact correction.
Often it is desirable to set the number of sweeps  and correction amplitude independently. For instance to reduce the noise one might want to increase  s  to  10  but keep the amplitude of  P over 4  (A_corr = .25) . The keyword  'CorrAmp'  lets you do this.

	Sweeps	:	N = s;	PoN = 1;	CorrAmp = ca ;

'ca'  is the magnitude of the correction sweeps relative to the first sweep.
Useful  values  for  'ca'  are from  .2  to 1.
FPulse rescales the sweeps internally so that the corrected result data on screen and in the file are the same as if computed without the  'CorrAmp'  keyword.
The  'Sweeps'  keyword can also be used when measuring non-pulsed data.
In this case there is no P over N correction, it is sufficient to specify
	Sweeps:	N = s
where s is any integer number >= 1. This determines how often exactly the same sweep (no correction) is repeated.  Alternatively

	Sweeps	:	N = s;	PoN = 0

can be written.
A sweep ends with the line

	EndSweep

In between there may be elements like Segment , Ramp , StimWave or Expo  which specify the shape, voltage and duration of the stimulus output. These elements can occur in (almost) any number and order and can be nested in loops. Having decided how the stimulation pattern should look like you put one element after the other by adding the approriate lines to your script file.


#### Frames
A sweep being repeated  s  times makes up a frame.
A frame can also be repeated a number of times, this is accomplished by enclosing the sweeps within the lines

	Frames:	N = f

where f is any integer number >= 1.
A frame ends with the line

	EndFrame

Frames have special properties. They allow automatic changing of script parameters ( see : Auto-increment  )  and also custom-changing ( see: Frame list ) of script parameters.

#### Auto-increment
Any automatic incrementing or decrementing of script parameters takes place on a once-per-frame basis. This has already been mentioned above for  'DAmp'  and  'DDur'  which automatically increase or decrease  the amplitude or duration of a  'Segment'  or a  'Ramp'  with every new frame, but is generally applicable for all  'Delta'  of all script parameters.

#### Frame list
In most cases a frame list can be specified instead of a single value. When advancing from frame to frame FPulse will use the appropriate values from the list for the stimulus.
The auto-increment example discussed  above in  Segment

	Segment :   Dac = 1, 10;	Amp	= -80,,10

giving a stimulus of  -80 mV in the first frame, in the next frame -70 mV, then -60 mV could be rewritten using a frame list

	Segment :   Dac = 1, 10;	Amp	= -80 | -70 | -60

Obviously a frame list requires some additional writing but is considerably more flexible as any values can be specified. If a value is to remain constant in succeeding frames it must be specified only once :

	Segment :   Dac = 1, 10;	Amp	= -80 | -70 | -60 | -60 | -60 | -50

can be formulated shorter as

	Segment :   Dac = 1, 10;	Amp	= -80 | -70 | -60 | | | -50

If the list has less entries than specified by  'Frames'  the last entry is repeated till the end.  Specifying more entries in the list than frames is flagged as an error.


#### Block
A frame being repeated   f  times makes up a block.
Unlike frames and sweeps a block cannot simply be repeated by just specifing 'N = repeats'.
Instead multiple blocks are defined by copying the code for each block (everything between and including the 'Frames'  and the  'EndFrame'  keyword) and appending it to the script. This has the advantage that stimuli can be constructed in multiple blocks differing much more than could be accomplished by using solely the  Auto-increment  or the  Frame list  feature.
A typical application could for example contain 3 blocks: pre-control, drug application and wash-out.

### Protocol
The script code of all catenated blocks (all lines between the 'Frames'  line of the first block and the 'EndFrame' line of the last block) is called a protocol. It can be repeated up to 999  times.  How often a protocol is repeated is not stored in the script but can be set in the  'Protocols'  field of the main panel.

Tip:	Set the protocol number to maximum if you want to run the acquisition 	continuously. You can stop the acquisition at any time by pressing 'Stop' .
	Do NOT use the lower left  'Abort'  button for this purpose.

The  'Protocols'  field is not editable at all times.

It is not editable
	- when a script is just loading
	- when an acquisition is running
	- when in  pulsed  Trigger Mode

It is editable before or after a script has been loaded.


### P over N
The sweep loop is introduced to allow correction of the capacitive artefact of  pulsed data. This correction procedure is called 'P over N' procedure and is abbreviated here as PoN. The first sweep in each frame (=the original sweep) is output as specified in the script, the remaining correction sweeps are derived from the first sweep, but automatically modified in amplitude in a manner that the PoN artefact correction will work.
The  PoN result sweep is stored in the CFS file additionally if sweeps is > 1.
There are two major working modes of FPulse: with P over N  correction off  or  on. The script keyword 'PoN'  allows to discriminate between the two modes.
The modes differ in how the script information is interpreted and in the way the acquired signal is corrected and stored.  The script syntax allows you to set the number of sweeps and the keyword PoN independently. This allows you to control the FPulse behavior in a specific manner.

1.	Sweeps = s  with s > 1  and  PoN is set to 1
 	Normal PoN mode
	Each frame will be repeated s times: once as original sweep and n-1 times as correction sweep,
	times the number of frames set by the Frames keyword.
	The PoN corrected result sweep is added to the CFS file.
	The PoN corrected result sweep is displayed during acquisition.

2.	Sweeps = 1  and  PoN is  not  specified  or  set to 0
	Normal  Spike / Synaptics  mode
	Each frame will be repeated once as the original stimulus specified in the script,
	times the number of frames set by the Frames keyword.
	There is no PoN correction.
	No PoN sweep is added to the CFS file.
	No PoN sweep is displayed during acquisition.

3.	Sweeps = 1  and  PoN is set
	Special mode (will issue a warning) : Do not do PoN correction but blow up CFS as if you had 	Each frame will be repeated once as the original stimulus specified in the script,
	times the number of frames set by the Frames keyword.
	There is no PoN correction.
	No PoN sweep is added to the CFS file.
	[ Other implementation: Instead of the PoN sweep an arbitrary data sweep is added to the CFS file.]
	The uncorrected sweep is displayed a second time during acquisition.

4.	Sweeps = s  with  s > 1  and  PoN is  not  specified  or  set to 0
	Special  mode (will issue a warning) : Do PoN correction but do not store results
	Each frame will be repeated s times: once as original sweep and n-1 times as correction sweep,
	times the number of frames set by the Frames keyword.
	Instead of the PoN sweep a sweep containing arbitrary data is added to the CFS file.
	[ Other implementation: No PoN sweep is added to the CFS file. ]
	No PoN sweep is displayed during acquisition.


### Trace Colors
The colors of the Adc, Dac or PoverN traces appearing during acquisition are specified in the script file. They can be controlled by the keyword RGB=( red, green, blue). Red, green and blue are numbers from 0 up to 65535 specifying the intensity of the color. Mixing colors is possible. Here are some examples:

	RGB = (  0, 0, 0 )	black
	RGB = ( 65535, 0, 0 )	bright red
	RGB = ( 30000, 0, 0 )	dark red
	RGB = ( 0, 30000,  0 )	dark green
	RGB = ( 30000, 30000, 0 )	brown
	RGB = ( 65535, 65535, 0 )	yellow
	RGB = ( 0, 65535, 65535 )	bright cyan
	RGB = ( 65535, 0, 65535 )	bright magenta
	RGB = ( 30000, 30000, 30000 )	grey
	RGB = ( 65535, 65535, 65535 )	white

The color of the stimulus shown when sript file is loaded or reread is not user adjustable in the same manner. The stimulus is colored automatically to give a hint on the time course of the protocol. The main stimulus of the first frame is shown in blue. Each following frame decreases the blue while adding some red. As the last frame is drawn last, its color will partially overwrite frames drawn before.
The correction pulses do not change colors with increasing frame number, they are always dark cyan. This color coding is only in effect when traces are drawn in a stacked mode. This mode can be set in the Panel 'Disp Stimulus' . Of course the color coding will only be visible if the stimulus changes somehow with increasing frame number.


Digital Output Pulses

The digital outputs are used to control devices with an on/off characteristic (e.g. switches, pumps, relays, trigger pulses ) . The timing is precisely matched to the Adc/Dac timing (better than 2us with a 1401plus).  A TTL pulse is output supplying between 4 and 5 V when on and less than about .5 V when off.

The syntax needed to introduce a digital output pulse is :

	Dig = channel1, duration1, delay1, delta duration1, delta delay1 [ [ / ch2,dur2,del2,ddur2,ddel2 ] / ch3...] ]

This is appended after the semicolon which finishes the Dac section of a script line.

A digital output pulse is characterized by
-	channel : 	can be 0 to 5
		channels 0 , 1 and  2 can be used without restrictions
		channels 3 and 4 are internally used to trigger the Adc- and Dac-conversion process.
		When using them you must take great care that they do not to interfere with this
		internal timing.
		You must NOT place the specification for channels 3 and 4 in the first line containing a Dac 		segment (or ramp, stimwave, etc) specification (that is the line behind the 'Sweeps:' 			keyword. You may place the specification for channels 3 and 4 in any other  following
		segment-type line and use a negative delay to compensate for the duration of the elapsed 			segments in between.
-	duration :	length of the positive pulse in milliseconds, must be a positive integral multiple >= 2 of the 			sample interval
-	delay :	length of the time in milliseconds which elapses until the pulse starts.
		This time is referenced to the time at which the Dac segment (or ramp, stimwave, etc.) in 			the same line (preceding this digital output specification) starts.
		Must be an integral multiple of the sample interval.
		Can be omitted if  0 and if the following deltas of this channel are also 0.
-	delta duration : if used the pulse length will increase or decrease from frame to frame by this value.
		Must be an integral multiple of the sample interval.
		Can be omitted if  0 and if the following delta delay of this channel are also 0.
-	delta delay : if used the time until the pulse starts  will increase or decrease from frame to frame by 			this value.
		Must be an integral multiple of the sample interval.
		Can be omitted if  0.

More channels can be appended after the separator  '/' .

There are a number of restrictions due to the Ced1401-internal handling of the digital time slices and due to the fact that the power1401 handles digital time slices differently than the rest of the 1401 family.
According to Tim Bergel of Ced (July 2003) this is caused by a hardware error in the power1401 which will be fixed.
The main restriction is that time slices must at least have a duration of 2 sample intervals. This restriction can be especially severe if more than 1 digital output channel is used, because the channels are not independent but internally mixed up into one 'super channel' containing the timing of all channels. The 'Two-time-slices-limitation' means that that there cannot be just 1 time slice between the pulse edges of any different channels. To make a long story short: the digital output channels are by no means independent but influence one another.
Another restriction stems from the fact that internally Dac output and digital output pulses have an internal delay of 1 sample interval. This is compensated for in the program but prohibits a digital output pulse to start at time 0, which is the time when the first Dac segment starts. However it may start at
- 1 sample interval  (effectively before the Dac starts, which might be a handy signal for an oscilloscope trigger) or at + 1 sample interval. Use the delay specification to control the start of the digital pulse.
Also the digital output pulse may only extend up to 1 sample interval before the end of the stimulus protocol ( 1 sample interval before the end of the last Dac segment ).
If any of these or other limitations prevent the digital output pulse from being output as specified in the script a warning ( level 'Severe' or 'Important' ) is issued once when the script is loaded. This is to inform the user that the digital output will not work as expected. The acquisition will take place nonetheless.


Note:
The two-time-slices-limitation may in principle (i.e. by a programming effort) be overcome by making the digital time slice half as long as the Dac/Adc interval.
This approach has not been taken for two reasons :
1.	Making the digital output processing internally twice as fast may have a negative impact on overall system performance, the maximum sample rate may decrease. (This has not been tested, though.)
2.	Only sample intervals with an even number of microseconds could be used. A sample rate of 40kHz would (as an example) be impossible.

	// Syntax:  [ Dig =  Ch, Dur, Del, DDur, DDel ]


Here's a more elaborate script containing most of the features descibed :

	PULSE
	Protocol:    Name = "DemoElaborate"
	Adc:         Chan = 0; SmpInt = 100; TGChan=5; Name=Adc0;  Units=pA
	Dac:         Chan = 0; SmpInt = 100; Gain =50; Name=Stim0; Units=mV
	PoN:         Src = 0; Units =pA

	// 1. Block
	Frames:      	N    = 2
	   Sweeps:     N    = 3 ;   PoN = 1
	      Segment:		Dac = 0, 1; Amp =80,1,-1;
	      Blank:		Dac = 0, 1; Amp=60
	      Blank:		Dac = 0, 2; Amp=40     	// ISI 1
	   EndSweep
	   Blank:			Dac = 0,3 ; Amp=30       	// IFI 1
	EndFrame
	Blank:				Dac = 0,3;  Amp=90      	// IBI 1

	// 2. Block without PoN
	Frames:      	N    = 1
	   Sweeps:     N    = 2
	      Segment:		Dac =  0, 1; Amp=15	 // ISI 2
	      Blank:		Dac =  0, 4; Amp=10	 // IFI 2
	   EndSweep
	EndFrame
	Blank:				Dac =  0, 5; Amp=0 	 // IBI 2


TODO  digout demo


### Trigger Mode
todo


##	FPulse Panels

Most user interaction take place in panels. Panels contain controls like checkboxes which allows turning some feature on or off. They may also contain input fields to enter text or fields for displaying some information. The  Status Bar and the Trace / Window Control Bar  are additional user interface components.  FPulse has about a dozen panels which are described below.

### FPulse
This is the main panel which can neither be minimized or hidden. It may go off screen, however, if you operate Igor in the 'windowed' mode (in contrast to the 'maximized' mode). Before you attempt to switch Igor to the  'windowed' mode you should drag the 'FPulse' panel to the middle of the screen so that it will stay accessible.

'Script file'	allows to select and then loads a new script defining a new stimulus and new data recording conditions.
	A file open dialog is displayed in which you can select the script file. The script file describes and controls the dac stimulus output, the digital output and the sampling of the adc channels.
	Depending on the number of data points this may take from fractions of a second to a minute.
	If the script could be read and interpreted without errors then the file name is displayed in the button and - if  'Dispay stimulus'  is on - then the stimulus and the digital output is displayed in the 'Stimulus' window in the upper left.
	Depending on the state of the 'Edit Script/Hide Script' button the script file will be hidden or displayed in a 'Script' window  below the 'Stimulus' window.  Editing is possible in the 'Script' window.
	The Dac stimulus output, the digital output and the sampling of the Adc channels will not yet start but will be prepared to start with minimal delay at the  'Start'  command.
	Three indicators tell you when loading is done: the 'busy' wheel in the lower left will disappear, in the history  'LOADING  IS  DONE... ' will be printed and the button title will change from  'Script file'  to the script name.
	See also:  Loading a script takes a very long time

'Apply'	reads the current script, builds and displays the stimulus. Apply is necessary after editing the script for the changes to come into effect.
	The script file will not be changed. The Dac stimulus output, the digital output and the sampling of the Adc channels will not yet start but will be prepared to start with minimal delay at the 'Start' command.

'Save'	the possibly changed current script file will be saved under the same script file name. The old script file contents will be overwritten.

'Save as'	prompts for a new name and saves the possibly changed current script under this name. The old script file contents remain unchanged.

'Edit Script/Hide Script'
	will open or hide a notebook window for editing or viewing the script. The window will be located below the 'Stimulus' window.  Screen space can be saved if the winjdow is hidden.

'Start'	starts stimulation and data acquisition.
	Until execution of the script is finished the  'Start'  button will be disabled and the  'Stop / Finish'  button will be enabled.
	When the script files commands are all processed i.e. when stimulation and acquisition have finished normally the button text will again be enabled.

'Stop / Finish'	The stimulation and the acquisition is in progress and can be interrupted by pressing this button.
	Note:    The system may not react immediately on pressing  'Stop'  so it is advisable 		              to wait some seconds before pressing the button again when nothing seems 		              to happen.
	Depending on the setting of the  'Append data'  checkbox different actions are taken:
	If   'Append data'  is off
	-  the script execution is stopped prematurely.
	-  the Cfs data file is closed but will be truncated.
	-  the next  'Start'  will write a file with the next auto-built name.
	If   'Append data'  is on  and the button title is 'Stop'
	-  the script execution is stopped prematurely.
	-  the Cfs data file is closed but will be truncated.
	-  the next  'Start'  will write a file with the next auto-built name.
	If   'Append data'  is on  and the button title is 'Finish'
	-  the Cfs data file will be closed normally after the end of the current
	   protocol  but acquisition will continue using the next auto-built name.

	Note:    Do  NOT  use  Igor's  'Abort'  button in the lower left for aborting a script.

'write file / watch'	button and indicator to switch and display whether a Cfs data file is written or not.
	In  'Write'  mode the CFS file is written. This is signaled by a green field.
	In 'Watch' mode stimulation and acquisition as defined in the script file are processed normally except for the fact that the CFS file is NOT written. This is signaled by the red field.

'Trigger mode'	Selects between software and hardware trigger.
	'Trig Start'  : 	stimulus and acquisition starts when  'Start' is pressed.
 	'Trig E3E4' : 	stimulus and acquisition starts when there is a
		TTL-Low-Pulse on the E3 and E4 input of the Ced1401.

'wait Start / wait Trigger'   todo
	indicator of current acquisition status and current  Trigger Mode  setting.

'Automatic backup'
	If  on  then every CFS data file is copied automatically to a file with the extension  'bak'  rather than  'dat' .

'Protocols'	determines how often the script is executed.
	There is no delay between one protocol and the next, so in principle exactly the same stimulus and Cfs data file can be achieved by copying the corresponding script lines multiple times and executing only 1 protocol. This is not an elegant solution.
	Depending on the setting  of 'Preferences' -> 'Append data...' only 1 long Cfs data file will be written for all protocols or  multiple short files, 1 for each protocol.

'Append data'	With this setting turned  off  a new data file with the next automatically built name is written whenever a new protocol is started.
	With this setting turned  on  data are appended to the data file opened when writing the first protocol until  'FPulse' -> 'Finish'  is pressed.
	Only then the current file is closed and the next protocol will be started with the next automatically built file name.

'Data' 	allows to change the directory where Cfs data file are written.
	Default is 'C:\Epc\Data\'

'Filebase'
'Cell'	These make up 2 parts of the Cfs data file name. The third part is an automatically built 2-letter combination ranging from  aa, ab, ac.... to  ..., zx, zy, zz. It is appended after 'filebase' and 'Cell'.
	The naming advances automatically whenever 'Start' is pressed (unless in 'Append data' mode ) so that no data are overwritten. The current automatic name is shown in the field below. It is updated after the first frame is written which may result in a noticable delay.

'Delete'	erases the current Cfs data file. If pressed multiple times any number of files going backward is erased.

'Acq windows'	opens  Disp Acquisition  panel for controlling the appearance of  the acquisition windows

'Stimulus'	opens  Disp Stimulus Panel  for controlling the appearance of  the stimulus window

'OLA analysis'	opens  OLA analysis  panel for setting Online Analysis options

'Data Utilities'
	opens  Data utilities  panel which allows to filter and extract data.
	You can also change axis scalings and to insert scale bars in a graph to fine-tune the graph before exporting or printing it.

'Preferences'	opens  Preferences  panel for setting general user preferences

'Miscellaneous'	opens  Miscellaneous Panel  containing controls which could not be attributed to any other panel.

'Comments'	allows to store additional text information the Cfs data file. The general comment is stored in the general section of the file, the specific comment is stored in the file section. The maximum length is 254 characters.

'Gain AxoGain'	opens  Gain Panel  for manually setting the gain of only those Adc channels whose Gain is not automatically transmitted to FPulse by a telegraph connection.

'Read CFS data'	opens  Read CFS data  panel for viewing and analysing acquired data.
	This panel offers a lot of options concerning the replay of sampled CFS data.


### Disp Acquisition
This panel allows to select those data which are to be displayed during the acquisition. By default FPulse initially starts in the automatic moded displaying all possible combinations of data sources, display ranges and display modes. This leads to a rectangular array of many small windows. In this panel you can turn off and on groups of windows,  load and save your preferred acquisition display window configuration and switch to an automatic window configuration. Every script has its own display configuration.

'Automatic data windows'
	all channels defined in the  Connection section  of the script appear as checkboxes. This can be Adc , Dac or PoN  channels.
	The checkboxes can be used to turn a data source completely on or off.
	One window row containing all ranges and all modes of this channel will be shown or hidden.  Usually the Dac channels are turned off.

'Range'	controls which episodes are displayed.
	Usually  Sweeps  or  Frames  are turned on. The  checkboxes  'Primary'  and  'Result'  are for the special case when acquiring pulsed data with
	P over N correction.
	'Primary'  displays only the first sweep (which is the measuring sweep) but hides correction sweeps.
	'Result'  displays the P over N corrected sweep which is the scaled sum of the 'Primary'  and the correction sweeps.

	todo
	PoN  must be turned on in the Connection section of the script.

'Mode'	in  'Current'  mode only the current data episode is shown. Before drawing new data the window is cleared.
	in  'Many superimposed'  mode the screen is not cleared before new data are drawn so that the traces accumulate on screen.

	Note:    'Many superimposed'  is somewhat dangerous if a large number of frames
	               is used.  The acquisition may fail if too much time is spent redrawing traces
	               which can easily happen as Igor redraws  ALL traces  in a window even if
	               only  ONE trace is added.

'Automatic'	build the rectangular array of all channels, ranges and modes which are turned on.

'user specific'	load preferred window configuration and arrange the windows in this order

'Save display config'
	save preferred window configuration

The fastest way to achieve a desired window configuration is start with the  'automatic'  configuration, then to blank out entire window groups by using the checkboxes and then removing from the few remaining windows those not needed with the upper right 'Close'  button.
The window(s) that are to be kept can be resized and dragged to the desired position.
'Preferences' -> Trace / Window Control Bar  allows to adjust the traces and axis within each acquisition window to your taste.
When the window arrangement is OK then the final step is saving the display configuration. Whenever you load this script it will come up with this display configuration.
Note:    	Drawing traces is time-consuming. To reduce the possibility of an acquisition 	failing and also to reduce the time lag between currently sampled and displayed 	data as little data as possible should be drawn during acquisition.

'Display all data even when lagging'
	Used to limit the time spent displaying the data.
	Acquiring the data and displaying the data are 2 different processes with different  priorities. When the attempted data rate is not too high for the system data will be displayed shortly after they have been acquired. When the data rate is too high, however, the data acquisition  being the high priority task will use up most of the time leaving no processing time for the display. In this case the display will lag the acquisition, meaning that whenever the screen is updated it is updated with data which may have been acquired some time ago. This can be quite confusing.
	The lag indicator in the  Status Bar  tells how much the display is behind.
	The described scenario takes place with this setting turned  on.
	Turning this setting  off  avoids time lags longer than 3 seconds by just skipping the display of data episodes if displaying them would increase the time lag beyond 3 seconds.

'High resolution during acquisition'
	Used to limit the time spent displaying the data.
	Displaying the acquired data can take up a considerable part of the total time leading to a lagging display or even a failing acquisition.
	This setting determines whether during acquisition every data point is displayed (high resolution but slow) or if some data decimation is applied to the displayed data. As in most cases many data points are mapped to 1 pixel this decimation will often not even be noticeable but generally there will be a respectable speed advantage which may avoid the time lag problem described above.
	For time-critical data acquisitions it is recommended to turn this setting  off  although the default setting is  on.
Note:   In 'Low resolution' mode short peaks may not reach their full amplitude on   screen.  This can be confusing during an online analysis as the correctly analysed peak amplitude will be indicated with a higher value.

'Trace / Window Controlbar'

'Prepare printing acq wnd'
	adds an informational textbox to any acquisition window and ensures that the the traces are plotted in high resolution mode. As these operations may slow down the acquisition process considerably they are turned off by default during acquisition. They are useful after acquisition when the graph is to be printed.


### Disp Stimulus Panel
The stimulus can be displayed before an acquisition is started for visually checking the pattern.
In this panel different views of the stimulus can be selected.
Two principal problems are encountered when displaying the stimulus.
First there is a huge discrepancy between the number of available screen pixels in X direction (in the order of 1000 to 2000) and the number of data points to be displayed. The latter is total protocol time divided by the sample interval. Data point numbers of  1 .. 10 million are quite common.
When trying to display more data points than the screen has X pixels short stimulus details will be suppressed. This can be quite confusing. To avoid this sort of display error for digital output pulses their length is stretched on the screen so that they are at least 1 pixel wide and will never be skipped.
The disadvantage of this approach is that expanding the stretched digital output pulse will give a wrong (too long) pulse length.
Note:  Do not attempt to read pulse durations of digital output pulses after having expanded (zoomed-in)  around the pulse.
The second problem can be the large amount of data to be displayed. To speed up the drawing process only every n'th data point is displayed. This decimation finally leads to the above problem.
The stimulus display options may help to partly 'work around' the problem but keep in mind that the displayed pulses will get coarser the more data points are drawn.

'Display stimulus'	allows to turn the stimulus display completely off.
	This can be advantageous after minor modifications to a known stimulus if displaying the stimulus takes long due to a large number of data points.
	Another reason for turning it off might be limited screen space.

'All blocks'	applicable only when there is more than 1 block.
	Display all blocks in catenated mode.

'Block'	applicable only when there is more than 1 block.
	Select the block to be displayed.

'all frames, all sweeps'
	display the whole block

'all frames, first sweep'
'one frame, all sweeps'
	display only parts of the stimulus

'catenate frames'
	display frames one after the other
'stack frames'
	display frames one on top of the other.
	In this mode the last frame will cover all previous ones if they are all the same.
	All frames will only be visible if they differ somehow.
	See: Auto-increment , Frame list

'include blank periods'
	periods which will not be stored can be excluded or included in the display.

'use same Y_axis for Dacs'
	applicable only when there are 2 Dacs or more having the same 'Units'.
	Normally Igor automatically scales data according to their minimum and maximum values. This may lead to slightly different axis for different Dacs.
	This checkbox (when on) rescales the data of 2 Dacs so that they share a common axis.


### Data Utilities
This panel contains functions which act on data in any graph.
You can filter data to remove noise, you can extract part of a trace later to be used as a stimulus or you can add a scale bar to a graph. Click into the desired window to make it the active graph before using these functions.
	See: Filter data
	See: Extract a StimWave
	See: Insert a Scalebar


### Preferences
This panel contains settings which effect the general FPulse behavior. The settings are not stored.

'Display all points (after acq, read CFS)'
	Used to limit the time spent displaying the data.
	Without limiting the number of data points to be displayed by decimation the time needed to build a trace in a graph can be annoyingly long if there are many data points. As described above a large speed advantage can be traded in against some signal fidelity when this setting is turned  off.

'Warning level'	controls how important a warning must be to be displayed in the history window.
	Set to  1 (minimum)  when only important errors are to be shown. This setting is recommended for experienced users.
	Set to  4 (maximum)  important errors but also warnings and messages are displayed.   This setting is recommended for novice users.
	The levels 2  and 3  range in between.

'Warning beep'	controls whether a warning is accompanied by the system sound.
	The beep can only be turned off  for warnings, errors always beep.

'Decrease Ced mem (MB)'
	Used to trade in script loading time against data rates.
	The physical memory of the Ced1401 is displayed in the input field.
	You can only decrease this value. If you leave it as it is you will be using the maximum memory possible. Using as much memory as possible allows maximum data rates but slows down script loading or applying. This input field allows to use only a part of the physical memory to speed up script loading or applying.
	See also:  Interpret memory partitioning information

'Max Reaction Time (s)'
	Used to trade in the delay in updating the acquisition display against data rates. Decreasing this value below .2 to .5 s will not have a big effect as other factors also limit the display update rate. Increasing this value to 1 s or higher will in many cases increase the maximum achievable data rate somewhat. The setting should only be changed if the data rate is to be pushed to the maximum.
	See also:  Interpret memory partitioning information
	See also:  Estimate the maximum achievable data rate


### Miscellaneous Panel
This panel contains features which do fit into any other panel or are used very seldom..

'Keywords and Defaults'
	prints a list containing all script keywords and some defaults in the history area.
	When there is no default  'NaN'  is printed.
	When multiple comma-separated values are expected  '(Nan)'  is printed.
	This is handy if you have forgotten the exact spelling of a keyword.
	Remember that also upper-case and lower-case is important.

'Test CED1401'	The  Ced1401 Panel  will be opened which lets you control the Ced1401 on a low-level basis.
	You should rarely need this functionality as  FPulse  controls the Ced1401 transparently.

'Kill all graphs'	This command deletes all graph window.	It might be useful in the interactive mode when Igor refuses to kill a wave because it is used in 1 or more graphs. After killing all graphs killing the wave will probably work. It will still be impossible it the the wave is also currently used in an XOP.

'Display raw (after acq)'
	Used to check quickly if the previous acquisition failed or is OK.
	If special test scripts are used this can be checked  at a glance.
	Displays the complete acquisition trace.
	See also: Estimate the maximum achievable data rate

'Quick check (after acq, TG)'
	Used to check quickly if the previous acquisition failed or is OK.
	If special test scripts are used this can be checked  at a glance.
	Displays the complete acquisition trace.
	See also: Estimate the maximum achievable data rate

'Show timing statistics'
	prints information after an acquisition how much time has been spent in various program parts. With this information the maximum data rate of the system can be estimated.
	See also: Estimate the maximum achievable data rate

'Search improved stim timing'
	Some limitations in the Ced memory management are contrary to the requirement of a high data rate. For this reason the optimum memory partitioning for data acquisition is neither trivial nor streightforward but is determined by searching. Unfortunately the optimum memory partitioning depends heavily but unpredictably on the number of stimulus data points. In an extreme case 2 data points more or less ( e.g. 200 us of a total stimulus length of  1000 s ) can make the difference between a failing and a successful acquisition. This command checks close-by stimulus data point numbers which might provide an improved memory partitioning without altering the stimulus timing too much.
	See also:  Interpret memory partitioning information
	See also:  Estimate the maximum achievable data rate


### Gain Panel
The Gain panel offers gain input fields for those Adc channels found in the script which have neither the keyword  'TGChan'  nor  the keyword  'TGMCChan' . Those channels require manually setting the gain in this panel so that it corresponds at all times to the value which is set on the AxoPatch gain switch.
Failing to do so will result in wrongly scaled Cfs data files. To avoid this possible error it is strongly recommended to let the AxoPatch telegraph its gain setting to FPulse.
To get a working telegraph connection for an AxoPatch200
  - 	connect a BNC cable between the AxoPatch telegraph output and an unused CED1401 Adc input
  - 	make this connection known by supplying the keyword TGChan = Ced1401 telegraph channel number
 	to the line of the controlled Adc in the Connection section
To get a working telegraph connection for an AxoPatch MultiClamp
- 	supply the keyword TGMCChan = AxoPatch channel number (=1 or 2)
 	to the line of the controlled Adc in the Connection section
The remaining control  'CC st'  is not functional yet.


### Ced1401 Panel
This panel contains commands for controlling the Ced1401 manually on a low-level basis.
Most commands were needed during the development and debugging phase but some of them might still prove beneficial.
The commands are self-explanatory.
The commands must be used with care as they interfere with the acquisition process.
Note:    	Never execute any Ced1401 command while an acquisition is running. Most 	probably the system will crash.
    	Also be sure to restore the Open / Close state of the Ced1401 to the state it had 	before executing these commands.



### OLA analysis
In the current FPulse version values for 'Base', 'Peak' and 'Rise' can be evaluated online during acquisition. Before the online analysis can take place the user must define regions in the trace where the data are to be evaluated.

	todo

Defining a region
Regions can only be defined in the acquisition windows containing traces so you may have to start at first an acquisition. You don't have to run a lengthy experiment, a single frame or sweep of acquired data may be sufficient (or you can stop the acquisition after the acquisition windows have been filled ).
To define a 'Base' region drag the mouse (left mouse button pressed) from the starting point to the ending point of the region while holding down the 'Control' key.
To define a 'Peak' region drag the mouse (left mouse button pressed) from the starting point to the ending point of the region while holding down the 'Shift' key.
To define any region drag the mouse (left mouse button pressed) from the starting point to the ending point of the region, click into the marquee rectangle and select the region type.
The defined region is displayed as a colored rectangle below the time scale. Base regions are blue, Peak regions are red and Rise regions are green. Only the displayed left and right borders (=the times) are meaningful, the height of the rectangle is arbitrary.


Status Bar
	todo

Trace / Window Control Bar
	todo

•	FEval Panels
Most user interaction take place in panels. Panels contain controls like checkboxes which allow turning some feature on or off. They may also contain input fields to enter text or fields for displaying some information.

Evaluation - Read CFS
	This is an obsolete panel. It is superseded by the Eval panel.

Eval Panel
This is the main evaluation as of 2005.

'Select file'	allows to select and then loads a data file for evaluation.
'Current acq'	selects the last recently acquired data file. The file is displayed in the input field below. This selection mode is differs from the way 'FPulse' looks for the current file.  In FPulse date and time of the file are ignored, instead the file with the highest auto-index (the 2-letter combination after the cell number) is considered to be the current file and the file with the next auto-index is written.
'< Prev file'	the file with the next smaller auto-index is selected and loaded.
'< Next file'	the file with the next larger auto-index is selected and loaded.
'Field'	displays path to the currently loaded file
'Sample interval'	the inverse sample rate of the currently loaded file in microseconds.

'1 col     all cols'	todo

'Graph'	open a popupmenu containing the available channels of the currently loaded file. The indexing goes up from 0 to the maximum number of channels - 1. This is NOT the true channel number (the true channel number is the number of the BNC plug on the Ced1401) .
	This popupmenu allows to view again channel windows which may have been closed, minimised or which may be hidden behind others.

'Display'	open a popupmenu containing viewing modes.
	Catenated displays all data segments with a true time scale.
	Stacked displays the data segments all starting at time zero.
	This mode is useful when an average trace is also displayed.

'Active'	selects a column of the 'Data sections' listbox which will be used by following commands.

'# Averaged'	displays the number of data units currently contained in the average.

'Erase Avg'	clears the average trace and sets '# Averaged' to zero.

'Save Avg'	saves the current average to disk. An auto-built name is constructed whose leading part is displayed in the text field below. Notice that the text displayed is only the leading part of the name. The complete file name additionally contains channel number and file extension. For example '_ch1.avg' may be appended to the partial file name displayed.

'Field'	displays the leading part of the auto-built file name for the NEXT average to be stored. The name is made up of the loaded data file (without extension) to which '#' and a one-character index is appended. The one-character index specifies the number of the average, not the number of the averaged traces. You may build multiple averages from the same data file by selecting different data segments to be averaged. The auto-built index goes from 0 to 9 and then from A to Z.  This imposes a limit of 36 averages with an auto-built name for each data file. It is the user's responsibility not to exceed this range.
	However, the field is editable so that another name can be chosen. For every new name the auto-index starts at '#0' making room for another 36 averages.

'Disp average'	hides or displays the average trace on screen.

'# Evaluated'	displays the number evaluations made in the current data file up till now.

'Disp unselected'	hides or displays the unselected traces on screen.

'Reset column'	resets the active column to the state before any averaging was done,
	clears any averages accumulated

Data sections
This panel is a 2-dimensional listbox for navigation through the currently loaded data file. It is constructed when a new data file is loaded.
The 'Data sections' panel allows to select parts of the data file for viewing, averaging and analysis.
The rows of the listbox are the data sections as stored in the Cfs file.
The columns are the larger data units as specified in the script.
A data unit may be a sweep, a frame, a block or a protocol.
The first column 'LSw' (LinearSweep) contains the data section number.
'Pro' is the protocol number.
'Blk' is the block number.
'Frm' is the frame number.
'Swp' is the sweep number.
'PoN' contains information if this sweep is the P-over-N-corrected data sweep.

When the user clicks into the panel the selected data unit is marked in the panel and displayed in the graph window. Depending on modifying keys ( Ctrl , Alt and CtrlAlt ) additional actions like analysing and averaging are taken.

Modifying key	State			Color
Alt			average			red
Ctrl		analysed		blue
CtrlAlt (the single key, not both Ctrl and Alt)	averaged and analysed	magenta
None		viewed and skipped	dark grey
Two further states are possible
before selecting the data unit	virgin			light grey
while the data unit is selected	current			apricot

The analysing and/or averaging is executed for the previous data unit.
In other words:
After you have clicked, selected and viewed a data unit you decide which action is appropriate.
While clicking into the next data unit for selecting and viewing you hold down the modifying key for appropriate action to take place for the previous data unit. When the analysis or the averaging of the previous data unit is finished it's color changes to display the new state both in the list box and in the graph.
Only now the currently selected data unit is displayed in the current color.

If no modifying key is held down no analysing or averaging will be done and the color will change to light grey.

The interleaving of analysing/averaging the previous data unit while selecting the next data unit may seem confusing at a first glance. However, after getting used to it processing the data can be done much faster than it could be done if the two steps had to be executed separately.

There are additional commands for selecting and processing the data:

Click the column header:	the complete column will be selected
Click the column header with modifying key:	the complete column will be analysed/averaged

Click into a data unit and then shift click
into another data unit:	all data units in between will be selected.
			You must stay within the same column.

There are 3 indicators to inform the user which data units and how many of them have been accumulated in the current average.
	- they are shown with the color code in the listbox
	- they are shown with the color code in the graph window
	- the  '# averaged'  field gives the number of averaged data units
It is possible to remove a data unit from the average. Just select the data unit  to be removed again, then select another data unit while NOT holding the 'Alt' modifier down.




##	HowTo

Start an acquisition
Press 'Start'.
This starts the data acquisition process as specified in the script file. If the CED1401 (the data acquisition hardware) is not connected properly, an error message is issued and the program is exited.

Depending on the settings in the script and in the various option panels a number of  'Auto' acquisition window will open and the acquired data will be displayed online.

The windows are called 'Auto' acquisition windows because FPulse displays the whole set of data acquisition windows automatically in a rectangular array of windows: data sources x ( interesting episodes + display modes )
Data sources are the DA channels, the AD channels and computed data channels (e.g. PoverN) as specified in the first part of the script file.
There are two kinds of interesting episodes: A frame consists of at least one sweep, common values are 4 to 20. The first sweep is the stimulus which evokes the response of the cell which we are interested in. The following sweeps of the same frame are correction sweeps, adjusted in timing and amplitude to the first sweep in a manner that allows to clean the first sweep from inevitable experimental artefacts.

This procedure is commonly called 'PoverN' correction, the corresponding cleaned trace in FPulse is called 'PoN'.

FPulse displays complete frames (the first sweep and all following correction sweeps) and indicates this by 'F' and just the first sweep 'S' .

There are also two kinds of display modes: current 'C' meaning that only the currently occuring sweep or frame is displayed or multiple 'M' meaning that all previous data are displayed together with the current data.
Each 'auto' data acquisition window can be identified by its name (first word in the graph window header) which is a combination of the data source name and the abbreviations explained above.


### Get multiple traces in one window
Assumption: there are acquired traces which are displayed in the acquisition windows
-	Click anywhere to the left of the left Y axis into the window from which you want to copy the traces
-	Press the keys Ctrl + Shift and keep them pressed
-	Drag (keep left mouse button pressed) from the point left to the Y axis in the source window into the 	target window and release the mouse button there
-	Release the  Ctrl + Shift keys
The new trace arrangement has been stored and will be effective during the next acquisition.

Save your display configuration / window arrangement
If you want to build your customized window arrangement from scratch, take the following steps:
-	Load the script for which you want a customized window arrangement
-	Open the panel 'Acq windows'
-	Select the data, the mode and the range to be included in your customized window arrangement
-	Press 'Automatic' if you have not changed any of the above options
-	'Run the script once to build the selected automatic windows and to fill them with data
-	Combine multiple traces in one or more windows. See also:  Get multiple traces in one window
-	Close all windows which you are not interested in by pressing the 'X' button in the upper right corner
-	Change the size and move the remaining windows to your liking
	(It is important to change the size of every window at least once, moving alone is not enough)
-	Press 'Save display config'


### Access traces in an acquisition window
Sometimes you may want to do specific operations on selected traces of an acquisition window. As a simple example you might want to display a selected trace in a new window using Igor's 'Display' command. For this to work you must know the tracce name. Pressing and holding the left mouse button down over the selected trace for about 2 seconds pops up a small information window on the lower left of the graph window containing the wave's name and the index and values at the point clicked. Unfortunately Igor does not supply the information in which folder the selected wave resides. In this version the folder is 	root:tmpacq:
so if the information popup window says something like
	Adc0FM_1[iii] :  x:jjj ,    y:kkk
you can display the trace by entering
	Display  root:tmpacq:Adc0FM_1

Another way to get the trace name is to press the right mouse button while being over the selected trace.

A menu will pop up also containing the trace names but still without folder information.
There are a number of drawbacks using any of these approaches:
-	if there are multiple traces in the window it may be difficult to position the mouse on the right trace
-	if there are multiple traces it is difficult to be sure that a desired trace (e.g. the 3rd frame) is selected
-	to speed up displaying the data some data points may be missing in the displayed data
-	the folder 'root:tmpacq:' may change in future releases.
It is also required that the desired trace is displayed, so this method will not work if the corresponding display window has been turned off.

To overcome these difficulties access the traces with the following trace access functions:

TraceF(   sTNm, fr )               will supply FRAME    trace name  for block 0  given base and frame
TraceP(   sTNm, fr )               will supply PRIMARY trace name  for block 0  given base and frame
TraceR(   sTNm, fr )               will supply RESULT   trace name  for block 0  given base and frame
TraceS(   sTNm, fr, sw )         will supply SWEEP   trace name  for block 0  given base, frame and sweep
TraceFB( sTNm, fr, bl )          will supply FRAME    trace name given base, frame and block
TracePB( sTNm, fr, bl )          will supply PRIMARY trace name given base, frame and block
TraceRB( sTNm, fr, bl )          will supply RESULT   trace name given base, frame and block
TraceSB( sTNm, fr, bl, sw  )   will supply SWEEP   trace name given base, frame, block  and  sweep

sTNm	is a string (enclosed in quotes!) containing the channel name e.g "Adc0"
fr		is the desired frame  number (counting goes from 0 to number of frames - 1 )
sw		is the desired sweep number (counting goes from 0 to number of sweeps - 1 )
bl		is the desired block  number (counting goes from 0 to number of blocks - 1 )

These functions return the trace as a string - NOT as a trace name, so you must convert this string
to a trace name by a leading  $  symbol.

Example:
You want to display the 'result' data (= the P over N corrected data) of the third frame of channel Adc0
Using the access functions you would have to enter:
	Display  $TraceR( "Adc0", 2 )


### Access traces in a ReadCfs window
For accessing traces after reading Cfs file data the same general considerations apply as for accessing acquisition traces.  See also:  Access traces in an acquisition window

The folder in the current version is
	root:cfs:
Again it is not guaranteed that this will not be changed in a future version.

Traces in ReadCfs can be accessed with the following functions:

TraceCfs( index, sw )            supplies SWEEP    trace name  for block 0  given channel index and sweep

index	is channel number in the Cfs file . It is counted up from 0 .
		If there are 2 channels then the upper window is channel index 0 and the lower is 1.
sw		is the desired sweep number (counting goes from 0 to number of sweeps - 1 )


Make an offline evaluation : Peak measurement
'Main Pulse panel'
	'Read Cfs data'
'Read Cfs Data panel'
	'Select file'  or  'Current acq..'
	'Single trace'
	'Mouse'	    on
	'Eval'	    on
	'Eval Panel'
'Evaluation panel'
	'Data channels'  : turn the desired channel on, all others off.
	'Region' : select 1 region, this is suffucient for 1 peak including the required base line.
	'Phase'  : select 1 if you want just the peak value, 2 if you additionally want to fit the peak.
	'Rn...' : select 'Rng:Cursor' . The peak will be searched within the range given by the cursors which you will set in a moment.
Select the peak direction.
	'Check ns' : Leave noise checking on.
	'Auto/User' : Leave  Auto/User   on.
Doubleclick on 1 of the red crosses.
If it gets selected drag it to the starting point of the peak region to be evaluated.
Doubleclick on the other the red cross.
If it gets selected drag it to the ending point of the peak region to be evaluated.



	'Same'   ,  '.Avg'  or  '.Next>'    etc. starts the evaluation.

TODO
...	'Save Avg'
'Evaluation  panel'
	'Show Average' on off on
The average file  having the extension 'avg' will be written in the 'Data' directory, usually 'Epc\Data'



Make an average current when analysing offline
'Main Pulse panel'
	'Read Cfs data'
'Read Cfs Data panel'
	'Select file'
	'Single trace'
	'Eval'	on
	'.Avg'  or  '.Next>'    etc.
	'Save Avg'
'Evaluation  panel'
	'Show Average' on off on
The average file  having the extension 'avg' will be written in the 'Data' directory, usually 'Epc\Data'



### Make an online analysis
Load a script

Start it (possibly reduced to only 1 frame) so that defining regions is easier

Adjust windows and combine traces to your liking.
Check the 'Frames' checkbox in the 'Disp acquisition' panel so that frames are the visible data unit.
Possibly run the script once so that data are drawn in the acquisition window.

Open the Online analysis panel.
	'Main Pulse panel'
 	    'OLA analysis'

Define 1 or more regions by dragging a rectangle in the acquisition window.
Define the type of the region by right clicking in the marquee rectangle.
Notice that the options in the 'Online Analysis panel' are geting filled as regions are defined.

To modify the analysis:
	'Online analysis panel'  :   select peak direction, decay fit function etc.

To view the online analysis results:
	'Online Analysis panel'    'Add Window'	( perhaps multiple times )
	In the OLA panel add OLA windows and select OLA results (e.g. peak, base) for each window.
	Connect a region to an analysis window by clicking the checkbox 'Display results in window'  off / on.

Edit the script and revert to the actually desired number of frames.
The analysis will take place once per frame, not once per sweep.

Start the acquisition.
	The result file having the extension 'ola' will be written in the 'Data' directory, usually 'Epc\Data'

If the data don't show up immediately click again twice the checkbox 'Display results in window'.

If the measured peak as indicated by the cross is not at the expected location then make sure that the peak direction is set appropriately and that the interval over which the peak amplitude is averaged is not too wide. You can set this interval in the input field 'average peak over ms' .

To remove a region : Click the checkbox 'Regions'  off
To remove the analysis window with the highest number: Click the button 'Delete wnd'
To remove all analysis data from all analysis window : Click the button 'Clear all'


### Interpret memory partitioning information
When a script has been loaded FPulse writes 2 lines in the history window supplying some condensed information about the current script and the memory partitioning.
The first line may look like
Ced ComputeTotalPoints()  computed 122000 points (= 12.200 s )  using a sample interval of  100 us  and  1 Protocol(s)   [Block(s):1  Frames(bl0):2  Sweeps(bl0):3]
It tells the sample interval, how many Dac points are contained in 1 protocol (the Adc points may be less if there are Blank sections) and how long it will take to execute 1 protocol.  It also tells how many protocols are required and how many blocks a protocol contains. For the first block the number of frames and sweeps is given.
The second line contains the information how the data points required by the script and the protocol setting are partitioned in the Ced memory. Though most FPulse users will never use this information is none the less necessary for those wishing to push the data rates to the system limit.
Ced CedStartAcq()   nReps:1  ChnkpRep:10  PtpChk:12200  DA:1  AD:2  TG:0  SmpInt:100us  Cmpr:122  ReactTm:1.2s  TA:13%  Mem:4%  FoM:15.4%
If the script is so short or the sample interrval so long or the number of protocols so small that all data points of the whole experiment fit into the Ced memory the achievable data rate depends only on the type of the 1401 . Transfer sppeeds and memory partitioning can then be ignored. In this case
 nReps:1
will be indicated. This one-repetition case will not be discussed further.
Only if
 nReps > 1
then data rates will be mainly limited by the transfer speeds which in turn depend on the Ced memory.
The total Ced memory ( as displayed initially in the field 'Preferences'  'Decrease Ced mem' ) is divided into 3 parts: Ced system memory (about 100kB) , Transfer area (=TA) memory up to 1 MB and user memory making up the rest.
In order to achieve high transfer speeds maximum transfer area usage is required. This condition is not fulfilled trivially as various other conditions concerning the integral division of data points and memory points must also be met.
The numbers indicate the repetitions (how often the user memory is filled), the chunks per repetition, the points per chunk, the number od Dac channels, the number of Adc channels, the number of telegraph channels and the sample interval.
Cmpr:122
is the telegraph data compression factor. The latter tells how many Adc points are sampled between 2 sampled telegraph data points. Telegraphs do not have to be sampled as often as Adc channels as their value changes seldom, this considerably reduces the amount of data which must be transferred.
ReactTm:1.2s
is the delay introduced by the chosen memory partitioning between sampling the data and displaying them. Other factors may increase this delay additionally.
The final three values indicate how well the memory is used. Good TA usage is of primary importance but (user) mem usage has (to a lesser degree) an effect on data rates as well. Both are combined in a somewhat arbitrary manner in a figure of merit which should be considered when comparing different memory partitioning.
TA:13%  Mem:4%  FoM:15.4%
The memory partitioning can be changed directly by adjusting
'Decrease Ced mem (MB)'
'Max Reaction Time (s)'
in the panel  Preferences  .
By executing
'Search improved stim timing'
in the  Miscellaneous Panel  minor changes to the script file are recommended to improve  memory partitioning.
But keep in mind that the mentioned tweaks will not improve the achievable data rate vastly, they are intended to make a failing acquisition work when it's demands are just a bit higher than the system limit.
Large improvements in the data rate can only be achieved with more powerful hardware.
See  Estimate the maximum achievable data rate  for measuring the system limit.

Estimate the maximum achievable data rate
In any data acquisition system the maximum achievable data is a key parameter. This rate is the sampling rate of the analogue-to-digital converters ( also the digital-to-analogue ) times the number of channels used. Other channels like digital outputs, digital inputs or counting inputs can be included.

The maximum achievable data rate depends on a variety of factors. The most important are
-	the revision of the CED1401plus EPROMS (MUST be 3.21 or later)
-	the speed of the computer
-	the amount of memory in the computer
-	the type of the CED (1401, 1401plus, U1410, Power1401)
-	the amount of memory in the CED
-	the type of interface between CED and computer (ISA, PCI or USB)

Apart from very simple rules ( "the higher the better"  and "use neither a standard 1401 nor an ISA card" ) it is very difficult to predict the actual achievable data rate.

Some measured rate limits (only to be used ase a very rough estimate) are given here.
	Pentium  233MHz  ISA interface  1401PLUS	 1 Dac channel    1 Adc channel	 		 10 kHz
	Pentium  350MHz  PCI interface  1401PLUS	 1 Dac channel    2 Adc channel	 		 40 kHz
	Pentium  1.8GHz   PCI interface  1401PLUS	 1 Dac channel    2 Adc channel  2 TG channel	 40 kHz
	Pentium  1.8GHz   PCI interface  POWER1401 1 Dac channel    2 Adc channel  		100kHz
	Pentium  1.8GHz   USB1 interf.   POWER1401 1 Dac channel    2 Adc channel  		100kHz


It is easier to estimate this rate by measuring it. Here it is how this is done.
Copy and paste the following script to your Scripts directory.
Proceed as described in the script.

	// Script:	DemoSpeedTest.txt
	//
	// Purpose:
	//	  Check if system samples the desired number of AD with the desired sample rate
	//    Possible sampling errors are detected easily and immediately by using ramps
	//    Different ramp times allow to check multiple closely spaced sample rates
	//		 1108.8 ms allows SmpInt  10,12,14,15,16,18,20,21,22,24,25,28,30,32,36,40 us
	//		 1801.8 ms allows SmpInt  10,12,14,15,16,18,20,21,22,24,25,26,28,30,36,40 us
	//	 	  982.8 ms allows SmpInt  10,12,14,15,16,18,20,21,24,25,26,27,28,30,36,40 us
	// Usage:
	//		- Copy this script using 'Save as' using a name which may include the SmpInt
	//		- Edit the script by selecting the desired AD channels, telegraph channels,
	//			sample interval and ramp time
	//		- Adjust the number frames so that the total duration approximately matches
	//			your real protocol or use integral multiples of the given ramp time
	//			If you increase the number of frames you will have to decrease the
	//			automatic increment proportionally to avoid an amplitude overrange
	//		- Connect DAC0 to all used ADC channels, connect telegraph channel(s) to the
	//			AxoPatch or another suitable source
	//	To achieve maximum speed:
	//		- Turn (most or all) acquisition windows off, turn 'timing statistics' on,
	//		- Turn  'Acq windows' 	 'Display all data even when lagging'	off
	//		- Turn  'Acq windows' 	 'High resolution during acquisition'	off
	//		- Turn  'Miscellaneous' 'Quick check (after acq)'				on
	//		- Turn  'Miscellaneous' 'Show timing statistics'				on
	//		- 'Start'  the acquisition
	//			Watch out for warnings or errors displayed in Igors history window
	//			Watch the 'Duty' indicator, it should stay below 80%
	//	After the acquistion:
	//		- Look for irregularities in the 'Quick check' window
	//		- Execute 'Miscellaneous' 'Display raw data'   and look for irregularities
	//		- Execute 'ReadCfs' , 'Protocol', 'Current Acq'  and look for irregularities

	PULSE
	Protocol:	Name	= "DemoSpeedTest25us"
	Dac:		Chan = 0;		SmpInt = 25; Gain = 1
	Adc:		Chan = 0;		SmpInt = 25; TGChan=5
	Adc:		Chan = 2;		SmpInt = 25; TGChan=6

	Frames:	N	=100
	Sweeps:	N	= 1
	Ramp:	Dac	= 0,  1108.8;	Amp	= -10000,1,100 // ramp time must match sample intervl
	Ramp:	Dac	= 0,  1108.8;	Amp	=    0
	EndSweep
	EndFrame


If the traces look OK and if you received no errors during the acquisition you can be confident that your system is capable to execute this script (and a similar one with the same length, channels and sample rate) successfully.
Unfortunately warnings stating that the sample rate is too fast will probably be issued. Experience shows that they can be ignored, only errors are an indicator of a failing acquisition.

If all went well you may want to try the next faster sample rate.

The indicator on the left side of the status bar gives you an early guess whether the acquisition will be successful or not.

Note: Executing a script to test the maximum data rates necessarily takes long. To get meaningful results the script must be so long so that the Ced user memory is filled multiple times, so there is no chance to speed up the measuring process.
See also:  Interpret memory partitioning information


### Filter data
Make the graph containing the trace(s) to be filtered the active graph by clicking into it.
Make sure that all points are displayed (even if it takes long) to avoid aliasing.
Open the  'Data utilities'  panel.
Select a  smoothing type  or a  filter type.
Select a filter frequency.
The filter frequency is ignored when smoothing. When smooting the indicated number of points is used.
Note:
GaussFFT	- is based on WaveMetrics sample code for a Gaussian filter using FFT and Inverse FFT.
		- can be quite fast
		- can be extremely slow (especially if number of data points contains big primes)
		- cannot filter integer waves
		- will give aliasing effects if the trace to be filtered has decimated data
		- the buffer end points are not treated satisfactorily

Gauss	- is based on the Sigworth and Colquhoun Gaussian filter algorithm
		- is rather slow (especially for a high number of data points and for heavy filtering)
		- will give aliasing effects if the trace to be filtered has decimated data
		- will fail for very heavy filtering (= very low cutoff frequency)

SmoothN	- is based on Wavemetrics Smooth operation with N passes. See: Operations[Smooth]
		- To be used for comparison as a starting point for more elaborate smoothing.


### Extract a StimWave
Display the graph with the wave which is to be converted to a StimWave .
Preferably the graph should display only this one wave.
The part of the wave to be extracted mustbe totally visible on screen.
Click into the graph to make it the active graph.
Press  'Data utilities'  ->  'Cursors Set'
An info panel and 2 cursors A and  B appear.
Select one or both by clicking the appropriate checkbox.
Move the cursor(s) to the begin and the end of the part to be extracted
This can be achieved by
-	dragging and dropping the cursors
-	dragging and dropping the vertical bar in the rectangle box
-	clicking in the rectangle box
-	pressing the horizontal arrows on the keyboard
Keep in mind that the begin of the extracted stimwave can not be changed later whereas the end can later be truncated.

Note :
If you want to avoid warnings when loading the script the number of points  extracted should be the same as the number required in the script ( duration divided by sample interval ).
If you want to avoid interpolation of the stimwave when it is inserted in the script it should have the same sample rate as the stimulus in the script.
Give the extracted stimwave a meaningful name (omit the extension IBW which is automatically appended).
The default name is shown in the box besides the 'Cursors Set/Cursors Cut' button and is constructed using the trace name and a date/time stamp.
Press  'Read Cfs Data'  ->  'Cursors Set'
The info panel and the 2 cursors disappear, the extracted stimwave will be written as an IBW file.
Move or copy the extracted wave into the directory where the scripts reside, usually 'C:UserIgor:Ced' .
Create or edit a script containing a line similar to

	StimWave:   Dac = 0, 10;    Wave  = MyStimWave.ibw, yScal, yOfs, xScl

The last 3 parameters are optional. They allow Y sizing, Y shifting and X sizing of the StimWave.
If omitted the default values are 1 , 0  and 1 .


Insert a Scalebar
	todo

Construct a sinusoidal wave

Paste the following code into the procedure window.

Function  ConstructSinWave()
//  Construct a sin wave
	variable	AmplitudeFactor	= 2
	variable	IntervalMs		= .2	// in milliseconds
	variable	DurationMs		= 10	// in milliseconds
	variable	FrequencyHz		= 200
	make	/O /N = ( DurationMs / IntervalMs ) 	SinWave
	SinWave = AmplitudeFactor * sin( p * 2 * pi * FrequencyHz * IntervalMs / 1000 )
	display	/K = 1 	SinWave
	modifygraph mode = 4			// lines and markers
	setscale 	/P x , 0, IntervalMs / 1000, "s" ,  SinWave
End

Execute the code by typing into the command line

	 ConstructSinWave() <ENTER>

You will perhaps have to modify the parameters of the sinusoidal wave according to your needs.

Note :
If you want to avoid warnings when calling the sinusoidal wave from a script the number of points should be the same as the number required in the script ( duration in script divided by script sample interval ).
If you want to avoid interpolation of the sinusoidal wave when it is inserted in the stimulus it should have the same sample rate as specified in the script.

To be able to use the 'SinWave'  as a stimulus from a script one further step is needed.

You must convert the sinusoidal wave into a 'StimWave' file.
Save the wave as an 'Igor Binary Wave' by typing into the command line

	 Save  SinWave <ENTER>

A  File Save Dialog box pops up allowing you to specify file name, file type (=file extension) and directory.
Be sure to select 'Igor Binary Wave' as a file type.
For the StimWave to be found when later called from a script it must be saved to the directory where the scripts reside, usually  'C:UserIgor:Ced' .

Using the same approach you can construct functions other than the sine wave to be used as a stimulus.


### Pitfalls

Stimulus is displayed with slight timing errors
This may become noticeable with stimuli containing many data points (more than about 50000). The apparent timing seems to be wrong by a few sample intervals (up to 1 ms or more). This is a display artefact resulting from the fact that only every n'th data point is displayed to save loading time.
The actual stimulus output by the Dac is exactly as specified in the script.

DAC pulse is longer than expected
This happens if the last line in the script sets the Dac to a value different from 0. This value is maintained until the Dac is set to another value, usually by starting the next sweep or frame. After the last sweep and frame follows no new value so the old value is kept until the Ced1401 is reset, which may take up to 1 second.
Cure: Insert a short (minimum 1 sample interval)  Segment or Blank with  Amp=0  after the last pulse,   or use the  InterSweepInterval  or the  InterFrameInterval  for this purpose.

When using multiple Dacs take care that no additional Dac exceeds its pulse duration to a time after the end of the sweep as detemined by the main Dac.


#### Multiple DACs
When using more than one DAC FPulse requires to
	- define a main Dac by placing its line before all other secondary DAC lines in the connection section
	- use the main Dac as a primary timing control of the multiple stimuli.
Timing of all secondary DACs is refered to the timing of the main Dac when relative timing is used.

See also: Absolute and relative timing of multiple DACs

Absolute and relative timing of multiple DACs
Absolute and relative timing must only be considered when using multiple DACs.
It is controled by the keywords 'A' and 'R' and determines the timing of the output pulses in the various DAC channels.
see:Multiple DACs
Absolute timing (keyword 'A') means that the timing of elements ( e.g. Segment, StimWave ) of the secondary DACs is referred to the beginning of the stimulus.  A positive delay following the keyword 'A' after the comma separator determines the time when the element is output to the secondary DAC.

Relative timing is (keyword 'R') in contrast to absolute timing ( keyword 'A' ) means that an element
( e.g. Segment, StimWave ) is output to the secondary DAC at the time when the preceding line's element output on the primary DAC0 is just finished. An additional delay is allowed following the keyword 'R' after the comma separator. The additional delay may be negative. This allows the secondary DAC output to occur at the same time or earlier than the primary DAC output.

Absolute and relative timing can be set differently for each secondary DAC line.
When deciding whether absolute or relative timing is more appropriate consider the following behavior: Absolute timing is refered to the beginning of the stimulus, not the frame. This means no matter how many frames you have defined an element with absolute timing will be output only once: in the first frame. If you want an element to be output in each frame then you must use relative timing.


#### Gain
To achieve meaningful numbers on the Y scales in the graphs FPulse must know the Y scaling factor of the patch clamp amplifier and of the CED. In this version of FPulse the scaling factor of the CED is assumed to be constant so the user does not have to worry about it. Things are not as easy concerning the patch clamp scaling factor.  The patch clamp scaling factor depends on the type of amplifier and on the mode setting.
Typical scaling factors and the appropriate Gain and Units settings in the scipt file are
	in the voltage clamp mode : 20 mV / V	Gain setting :  50		Units : mV
	in the current clamp mode :  2 nA / V	Gain setting :  0.5 		Units : nA
or
	in the current clamp mode :  2000 pA / V	Gain setting :  0.0005 		Units : pA

These values must be set in the Connection section of the script file.

#### Gain: manual setting
Additionally the front panel gain setting of the patch clamp amplifier must be taken into account. This setting can be incorporated in the script file gain setting or it can be set in FPulse in the main control panel.

#### Gain: automatic setting using the patch clamp amplifiers telegraph outputs
The front panel gain setting of the patch clamp amplifier  AxoPatch 200  can automatically be transmitted to FPulse. For this mechanism to work you must electrically connect the patch clamp amplifier's telegraph output to an unused Adc input of the CED and you must let FPulse know about this connection. You do this by inserting the TGGain keyword in the line in the IO section of the Adc whose gain is to be transmitted. Although you are free to connect any Adc channel to any telegraph channel the following combinations are standard connections:

	Adc:	Chan = 0; SmpInt = ... ;  Gain = ... ; Units = ... ; TGGain = 5
	Adc:	Chan = 7; SmpInt = ... ;  Gain = ... ; Units = ... ; TGGain = 6

If you are using a MultiClamp 700 no hardwired connections are necessary for telegraphing the gain from the  patch clamp amplifier to FPulse. The communication is entirely controlled by software. To tell FPulse to look for gains coming from a MultiClamp 700 you must use the TGMCChan keyword:

	Adc:	Chan = 0; SmpInt = ... ;  Gain = ... ; Units = ... ; TGMCGain = 1
	Adc:	Chan = 7; SmpInt = ... ;  Gain = ... ; Units = ... ; TGMCGain = 2



IO section keywords

	PULSE
	Protocol:	Name
	Adc:	Chan = ...; SmpInt = ... ;  Gain = ... ; Units = ... ; TGGain = ...
	Adc:	Chan SmpInt Gain Units Name TGChan RGB;4;(48000,0,8000)"
	Dac:	Chan SmpInt Gain Units Name RGB;4;(12,34000,56)"
	PoN:	Src Units RGB;4;(0,0,50000)"


#### PoverN
If you want PoverN correction to be executed and PoN corrected sweeps to be stored in the Cfs file then you must specify  PoN=1  after the Sweeps keyword  AND ALSO  you must specify Pover in the IO section of the script, e.g.   PoN : Src = 0 meaning that data from Adc channel 0 will be PoverN corrected.


#### Filtering does not work as expected
For the Gaussian filter to work there must be no decimation in the data to be filtered.
All data points must be displayed to avoid aliasing artefacts.
If you are filtering traces in Acquisition windows: Turn on 'Disp Acquisition' -> 'High resolution during acq'
If you are filtering traces in ReadCfs windows:     'Preferences' -> 'Display all points (aft acq, read CFS)'

### Problems

#### Adc trace is garbage but Dac trace looks OK
When the acquired Adc traces do not at all look as you expected (the amplitude may be OK but the timing seems to be wrong), you are probably sampling too fast or too many channels or both. This error can be observed during acquisition. The timing of the Adc traces should resemble that of the Dac traces. If you suspect an Adc timing error and the amplitude of the Adc signal is so confusing that you cannot clearly detect it, the disconnect your Adc signal an connect the stimulus Dac channel instead. Restart the acquisition and compare the Adc to the Dac timing. Now the shape of the the signal in the acquisition window of the Adc should resemble the shape observed in the window of the Dac. If not you are probably
demanding more than your system is able to supply. Then you should
reduce the number of channels and / or reduce the sample rate (increase the sample interval).
If the timing differences suddenly disappear, i.e. the Adc and Dac timing looks alike, then you just estimated the maximum achievable data rate on your machine.
See also: Estimate the maximum achievable data rate
Hints for pinpointing down a timing error:
1. Approximately keep the total time of the script, but put it into just two equal length segments.
2. Round the segment time to a nice number, e.g. 5750 ms to 6000ms.
4. Keep in mind that the segment time must divisible by the sample intervals to be checked.
5. Use 6 to 20 frames, and 2 sweeps, again keeping the total time approximately constant.
6. Use a segment amplitude of 0 for one of the two segments.
7. For the 2. segment use an amplitude large enough so that the Adc signal fills the graphs nicely.
8. For the 2. segment use an automatic ampl decrement of 10% to 4%, depending on the frame number.

Notice that the duty indicator does not indicate all types of timing errors. It detects errors in the most problematic timing bottleneck: the continuous passing of the data from the CED buffer over Windows into IGOR. For some (unknown) reason it does reliably detect timing errors if just the CED interface is too slow, which is often the case if an ISA interface is used. The cure for this type of timing error is simple: use a PCI interface. You can expect to achieve sample rate 2 to 3 times faster.
If you are not sure which type of interface you are using, run (from DOS) the program CedMenu and select 'Report'. The program is usually located in the directory  C:\1401\Utils\CedMenu.exe .


#### Running a new script for the first time
If you have written a new script and want to run it for the first time, it is wise to turn most of the acquistion windows off (->Acquisition windows options) and to turn off the  ->High resolution during acq.  Then watch the duty indicator during the acquisition. If it stays in the green range you are on the safe side (your system has spare power) and you may perhaps want to turn more windows and/or the high resolution mode on again.  Even if the duty indicator goes into the red range (appr. more than 80% duty) your system might still work correctly meaning there are no missing or corrupted periods in the acquired data.
But there are 3 disadvantages working in that range:
 -	the display windows are not updated in time, they seem to be frozen
 -	the system reacts neither on the IGOR Abort button nor on the FPulse Start/Stop button.
 -	the acquisition process can take so many times longer than predicted by the script (say 20 times)
	that you may get the impression that the system crashed.
It probably has NOT crashed but is still busy processing the rest of the data which have been acquired a long time ago.
It is also a good idea to leave the button 'Preferences'  'Disp all data even when lagging' unchecked.
This is especially important when the new script has many fames, i.e. more than 50 or 100.
Maybe in a future version FPulse will be able to adapt automatically to over-ambitious user settings to avoid the above disadvantages.....



#### The acquisition process takes much longer than the script specifies

This can occur if your script and your settings are too demanding for the given computing power. Displaying the data during acquisition can require more computing power than you actually have, so the display falls back in respect to the acquisition. The acquisition may have finished some time ago but there are acquired data waiting to be display: the display lags the acquisition.
The status line a couple of numbers are shown indicating whether this is the problem or not.
The indicator 'Lag' says how many seconds the display is behind the acquisition, ideally it should be less than a few seconds. The indicators 'Swps' tell you how many sweeps have been displayed and how many sweeps must be displayed as specified in the script. The indicators 'Time' display the elapsed time during this acquisition and the total time as specified in the script. If the elapsed time has passed the total time and the displayed sweeps have not yet attained their end value, your display is definitely lagging the acquisition. This is also shown in the 'Lag' indicator.

The cure for this problem is manyfold:
-	display in 'current' mode rather than in 'Many superimposed' mode
-	display less acquisition windows
-	display less traces
-	try to decrease the number of frames in your script
All these factors influence the computing time for the display heavily, especially the first point, the 'Mode'.

You can also uncheck the button 'Preferences'  'Disp all data even when lagging' . Then data to be displayed will simply be skipped when the lag approaches a certain limit, currently 5 seconds.
This guarantees that your display will never be more than 5 seconds behind the acquisition no matter how many data you are trying to display on an inadequate computer. Of course data to be written to disk will never be skipped.


#### Loading a script takes a very long time

Loading a long script ( 10 000 000 points or more ) generally takes some ten seconds on a 2GHz Pentium. The biggest part of this time is needed to transfer as much of the stimulus to the Ced1401 as possible before the acquisition starts. The advantage of doing so is that less stimulus data have to be transferred during the acquisition giving a higher achievable data rate : more channels can then be sampled with a higher data rate over a longer period of time. If the highest achievable data rate is not of prime importance limiting the amount of the stimulus transferred to the Ced during loading may be used to cut down the loading time.
This is done by simulating a smaller Ced memory : 'Preferences' -> 'Decrease Ced mem (MB)'
Finding a compromise between load time and achievable data rate may require some experimenting.
Loading time can also be decreased considerably by not displaying the stimulus.
'Disp Stimulus' -> 'Display stimulus: OFF' does this. Sometimes it may be sufficient just to exclude the non-stored periods from the display : 'Disp Stimulus' -> 'include blank periods: off' .
In some cases the time needed to clean up the screen for a new script may also be decreased.
See also:  Loading a short script takes a long time

#### Loading a short script takes a long time

The time needed to clean up the screen for a new script may be quite long if the previous script had many frames and if all of them were displayed in the stimulus display and / or during the acquisition. Each displayed frame is internally represented as a wave. Before a new script can be loaded all the old waves must be removed from memory which may take some time.
The solution is to display less frames:
-	Set 'Stimulus disp options' 'one frame...'
-	Display during acquisition in 'Current' mode rather than in 'Many superimposed' mode
-	Display less windows during acquisition
-	Uncheck  'Preferences'  'Disp all data even when lagging' .




#### Loading a script takes a long time even when the script is short

This can occur if your previous script had many frames and if had displayed all of them in the stimulus display and / or during the acquisition. Each displayed frame is internally represented as a wave. Before a new script can be loaded all the old waves must be removed from memory which may take some time.
The solution is to display less frames:
-	Set 'Stimulus disp options' 'one frame...'
-	Display during acquisition in 'Current' mode rather than in 'Many superimposed' mode
-	Display less windows during acquisition
-	Uncheck  'Preferences'  'Disp all data even when lagging' .




There are steps visible on the slope of a ramp
See the script 'DemoBadSteps.txt'

##### Problem:
You will notice annoying steps (where you expected to see a smooth ramp)

Explanation:
In the Current Clamp mode the AxoPatch has a gain of 2 nA / V = 2 pA / mV. To compensate for this you set the DAC gain to .5 allowing you to write the script Amp directly in pA. For the 200 pA ramp desired below the Ced1401 will output a 100 mV ramp. For this the Ced1401 uses his 12bit Dac with 2^12 = 4096 steps spread over a range of -10 to +10 V, leaving appr.  5 mV  stepsize  or about  20 steps of appr.  10 pA on the desired 200 pA ramp.

This is NOT an error, the system hardware (Ced1401 and AxoPatch) is designed this way.

Solution1:
Use a Ced1401 model with a 16 bit Dac. The steps will still be there but smaller by a factor of 16 and will be appr. .6 pA (probably obscured by noise).

Solution2:
Scale down the stimulus signal between Ced1401 and the PatchClampAmp with a resistive divider, e.g. by a factor of 10. You then have 2 pA / 10mV and you must change the DAC Gain from .5  to  5 .
The Ced1401 will still output 5 mV steps but spread over a ramp of 1000 mV.
The steps will still be there but smaller by a factor of 10 and will be appr. 1 pA (probably obscured by noise).


•	Bugs , Flaws and Problems
The same applies for Trace access functions in ReadCfs.

•	Limitations and missing features
Online evaluation
The Online evaluation is not block-aware. The evaluation parameters (e.g. region begin, end, Base or Peak, peak direction, type of decay fit function) selected for the first block also apply to all following blocks.
You cannot do Online evaluation on a per-sweep basis, only on a per-frame basis.
Defining evaluation regions in the 'Sweep' acquisition windows will lead to confusing results.
You should define evaluation regions in the 'Frame', 'Primary' or 'Result' acquisition window.
When not using PoverN you can imitate an evaluation on a per-sweep basis by setting the number of sweeps per frame to 1 .



•	Dont's
Do not use the  '|'  character  in channel names , script names  or file names
Do not use the '_'  character  in channel names , script names  or file names
Do not use the '~'  character  in channel names , script names  or file names


•	Sample Topic
Flags
What	Allowed in Functions?	Comment
Assignment statements	Yes	Includes wave, variable and string assignments.
Built-in operations	Yes, with a few exceptions.	See Operations in Functions for exceptions.
