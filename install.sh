#!/bin/bash 
# 
# FPulse Installer
#     installs FPulse 3.45 on IgorPro 6, running on Linux/Wine 
#     Although, this is not sufficient for data acquistion, 
#     this is good enough to install FPulse, and get access to the 
#     documentation and help (reading FPulse.ihf) 

# Copyright (C) 2021,2024 Alois Schlögl, IST Austria

# make install run as admin
# https://developpaper.com/how-to-make-bat-batch-run-with-administrators-permission/
#    4. Automatically run batch (BAT) files as Administrator

# source directory of FPulse
SRCDIR=$(dirname ${0})

if [[ -z $WINEPREFIX ]]; then
	echo "ERROR: WINEPREFIX not defined"
	exit -1
	WINEPREFIX=$HOME/.wine
	wineboot -u 
fi

# installation directory of FPulse
DESTDIR="${WINEPREFIX}/drive_c/UserIgor/FPulse"

# Directory to Igor Pro User files - here are some examples
IPUF="$WINEPREFIX/drive_c/Users/$(whoami)/Documents/WaveMetrics/Igor Pro 6 User Files/"
IPUF="$WINEPREFIX/drive_c/Program Files (x86)/WaveMetrics/Igor Pro Folder/"
if [[ ! -d $IPUF ]]; then
	echo "ERROR: Igor-pro-user-files-folder not found"
	exit -1
fi

install() 
{
	echo "=== Copying Files into %DESTDIR% ==="
	mkdir -p "${DESTDIR}/XOPs/"
	cp -r "${SRCDIR}/UserIgor/FPulse"  "$(dirname ${DESTDIR})" 
	cp "${SRCDIR}/install.sh"          "${DESTDIR}"/uninstall.sh
	cp "${SRCDIR}/UserIgor/XOP_Axon/FP_Mc700Tg/vc2015/FP_Mc700Tg.xop"  "${DESTDIR}/XOPs/"
	cp "${SRCDIR}/UserIgor/XOP_Ced/FPulseCed/vc2015/FPulseCed.xop"     "${DESTDIR}/XOPs/"

	echo "=== Install DLLs and remove CFS32.dll, (need elevated permissions) ==="
	rm -f "${WINEPREFIX}/drive_c/windows/syswow64/CFS32.dll"
	rm -f "${WINEPREFIX}/drive_c/windows/syswow64/Use1432.dll"
	cp "${SRCDIR}/UserIgor/XOP_Dll/AxMultiClampMsg.dll" "${WINEPREFIX}/drive_c/windows/syswow64/"

	echo "=== Create Links for Igor ==="
	# Syntax: mklink [[/d] | [/h] | [/j]] <link> <target>
	ln -sf "${DESTDIR}/XOPs/FP_Mc700Tg.xop"		"${IPUF}/Igor Extensions/FP_Mc700Tg.xop" 
	ln -sf "${IPUF}/More Extensions/Data Acquisition/AxonTelegraph.xop"	"${IPUF}/Igor Extensions/AxonTelegraph.xop" 
	ln -sf "${DESTDIR}/XOPs/FPulseCed.xop"		"${IPUF}/Igor Extensions/FPulseCed.xop"  
	ln -sf "${DESTDIR}/FPulse.ihf"			"${IPUF}/Igor Help Files/FPulse.ihf"     
	ln -sf "${DESTDIR}/FPulse.ipf"			"${IPUF}/Igor Procedures/FPulse.ipf"     
	ln -sf "${DESTDIR}"				"${IPUF}/User Procedures/FPulse"         
}


uninstall() 
{
	echo "=== Uninstall Igor links %IPUF% ==="
	rm -f "${IPUF}/Igor Extensions/FP_Mc700Tg.xop"
	rm -f "${IPUF}/Igor Extensions/FPulseCed.xop"
	rm -f "${IPUF}/Igor Extensions/AxonTelegraph.xop" 
	rm -f "${IPUF}/Igor Help Files/FPulse.ihf"
	rm -f "${IPUF}/Igor Procedures/FPulse.ipf"
	rm -rf "${IPUF}/User Procedures/FPulse"
	echo "=== Remove DLL's from CED and MultiClamp (need elevated permissions) ==="
	rm -f "${WINEPREFIX}/windows/syswow64/Use1432.dll"
	rm -f "${WINEPREFIX}/windows/syswow64/CFS32.dll"
	rm -f "${WINEPREFIX}/windows/syswow64/AxMultiClampMsg.dll"
	echo  "=== Uninstall $DESTDIR ==="
	rm -rf "${DESTDIR}"
}

case ${0} in
uninstall.sh | *uninstall.sh) 
	uninstall
	;;
install.sh | *install.sh) 
	if [[ "${1}" == "-u" ]]; then 
		uninstall
	else	
		install
	fi
	;;
esac

echo "execution completed, any key to exit"

